<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Articles</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
	            <div class="col-lg-12">
	            <ol class="breadcrumb">
                	<li><a href="<c:url value="/article/nouveau" />" ><i class="fa fa-plus  "></i> Ajouter</a></li>
                </ol>	            
	            </div>
            
            </div>
        <div class="row">
               

                        <div class="row">
                        <ul class="nav nav-tabs " style="margin: 10px;">
                                <li class="active"><a href="#profile" data-toggle="tab"><i class="fa fa-th-list  "></i></a>
                                </li>
                                <li ><a href="#home" data-toggle="tab"><i class="fa fa-th-large "></i></a>
                                </li>
                            </ul>
                          </div>
                            <div class="tab-content">
                                <div class="tab-pane fade" id="home">
                                <table width="100%" class="dataTables">
                                <thead hidden="true">
                                    <tr>
                                        <th>Article</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${articles}" var="article">
                                 <tr class=" col-md-4" >
                                 		<td width="100%">
                                 		<div class="panel panel-default row">
                                 		<div class="panel-heading"><b>${article.getCodeArticle() }</b></div>
                                 		<div class="panel-body">
                                 		<div class=" col-md-4">
                            			<img src="<%=request.getContextPath() %>/images/article/${article.getPhoto() }" width="70px" height="90px"/>
                            			</div>
                            			<div class=" col-md-8">
                            			Cat : <b>${article.getCategory().getCode() }</b><br>
                            			Prix HT : <b>${article.getPrixUnitaireHT() } DH</b><br>
                            			TVA : <b>${article.getTauxTva() } %</b><br>
                            			Prix TTC : <b>${article.getPrixUnitaireTTC() } DH</b>
                            			</div>
                            			</div>
                        				<div class="panel-footer"> <c:url value="/article/modifier/${article.getIdArticle() }" var="urlModifier"/>
										<a href="${urlModifier }" class="btn btn-primary"><i class="fa  fa-edit"></i></a>
										&nbsp;|&nbsp;
			                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal_${article.getIdArticle() }"><i class="fa fa-trash-o "></i></button>
			                            
			                            <!-- Modal -->
			                            <div class="modal fade" id="modal_${article.getIdArticle() }" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                                <div class="modal-dialog">
			                                    <div class="modal-content">
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                            <h4 class="modal-title" id="myModalLabel">CONFIRMATION!</h4>
			                                        </div>
			                                        <div class="modal-body">
			                                        <div class="row">
			                                        <div class="col-xs-2">
			                                        <i class="fa fa-warning fa-4x "></i>
						                            </div>
						                            <div class="col-xs-10">
						                            Voulez-vous vraiment supprimer l'Article : <br>
						                            <img src="<c:url value="/files/${article.getPhoto() }" />" width="50px" height="50px"/>&nbsp;<b>${article.getCodeArticle() }</b>					                            
 						                            </div>
			                                        </div>
			                                         </div>
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			                                            <c:url value="/article/supprimer/${article.getIdArticle() }" var="urlSupprimer"/>
			                                            <a href="${urlSupprimer }" class="btn btn-danger"><i class="fa fa-trash-o "></i> Spprimer!</a>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div></div>
                        				</div>
                                 		</td>
                                    </tr>
                                    
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                                </div>
                                <div class="tab-pane fade in active" id="profile">
                                <table width="100%" class="table table-striped table-hover dataTables">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Designation</th>
                                        <th class="text-center">Categorie</th>
                                        <th class="text-center">Entrepot</th>
                                        <th class="text-center">Stock Actuel</th>
                                        <th class="text-rigth">Prix TTC</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${articles}" var="article">
                                <c:url value="/article/details/${article.getIdArticle() }" var="urlArticler"/>
                                
                                 <tr>
                                        <td class="clickable-row"  data-href="${urlArticler }" style="cursor: pointer; "><b>${article.getCodeArticle() }</b></td>
                                        <td class="clickable-row"  data-href="${urlArticler }" style="cursor: pointer; ">${article.getDesignation()}</td>
                                        <td class="text-center clickable-row"  data-href="${urlArticler }" style="cursor: pointer; ">${article.getCategory().getCode() }</td>
                                        <td class="text-center clickable-row"  data-href="${urlArticler }" style="cursor: pointer; ">${article.getEntropot().getCode() }</td>
                                        <td class="text-center clickable-row"  data-href="${urlArticler }" style="cursor: pointer; ">${article.getStock() }</td>
                                        <td class="text-right clickable-row"  data-href="${urlArticler }" style="cursor: pointer; ">${article.getPrixUnitaireTTC() }<b> DH</b></td>
                                 
                                        <td class="text-center ">
                                        <c:url value="/article/modifier/${article.getIdArticle() }" var="urlModifier"/>
										<a href="${urlModifier }" class="btn btn-primary btn-circle"><i class="fa  fa-edit"></i></a>
										&nbsp;|&nbsp;
			                            <button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#modal1_${article.getIdArticle() }"><i class="fa fa-trash-o "></i></button>
			                            
			                            <!-- Modal -->
			                            <div class="modal fade" id="modal1_${article.getIdArticle() }" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                                <div class="modal-dialog">
			                                    <div class="modal-content">
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                            <h4 class="modal-title" id="myModalLabel">CONFIRMATION!</h4>
			                                        </div>
			                                        <div class="modal-body">
			                                        <div class="row">
			                                        <div class="col-xs-2">
			                                        <i class="fa fa-warning fa-4x "></i>
						                            </div>
						                            <div class="col-xs-10">
						                            Voulez-vous vraiment supprimer l'Article : <br>
						                            <img src="<c:url value="/files/${article.getPhoto() }" />" width="50px" height="50px"/>&nbsp;<b>${article.getCodeArticle() }</b>					                            
 						                            </div>
			                                        </div>
			                                         </div>
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			                                            <c:url value="/article/supprimer/${article.getIdArticle() }" var="urlSupprimer"/>
			                                            <a href="${urlSupprimer }" class="btn btn-danger"><i class="fa fa-trash-o "></i> Spprimer!</a>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
										</td>
                                    </tr>
                                    
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                                </div>
                            </div>
                                  

            </div>

        </div>
    </div>
    <script >
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
    </script>
    

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   