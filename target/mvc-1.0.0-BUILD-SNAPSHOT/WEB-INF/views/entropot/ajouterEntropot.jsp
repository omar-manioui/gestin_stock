<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Entropots</h1>
                </div>
            </div>
            
        <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Nauveau Entropot
                        </div>
                        <div class="panel-body">
                        
                            <c:url value="/entropot/enregistrer" var ="urlEnregistrer" />
							<f:form modelAttribute="entropot" action="${urlEnregistrer }" role = "form">
							<f:hidden path="idEntropot"/>
						  <div class="form-row form-group col-md-10">
						     <label>Nom d'Entropot</label>
                             <f:input path="code" class="form-control" placeholder="Nom d'Entropot" />
						  </div>
						  <div class="form-group form-row col-md-10">
						    <label>Designation</label>
                             <f:input path="designation" class="form-control" placeholder="Designation" />
						  </div>
						  
                            
                        </div>
                        <div class="panel-footer">
                        <div class="row">
                        <div  class="col-md-3"></div>
                        <div  class="col-md-6">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Enregister</button>
                            <a href="<c:url value="/entropot/" />" class="btn btn-danger"> <i class="fa fa-arrow-left"></i> Annuler</a>
                        </div>
                        <div  class="col-md-3"></div>
                        </div>
                        </div>
                        </f:form>
                    </div>
                </div>
            </div>
        </div>
    </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   