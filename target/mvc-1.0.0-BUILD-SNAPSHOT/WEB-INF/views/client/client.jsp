<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Agents</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
	            <div class="col-lg-12">
	            <ol class="breadcrumb">
                	<li><a href="<c:url value="/client/nouveau" />" ><i class="fa fa-plus  "></i> Ajouter</a></li>
                </ol>	            
	            </div>
            
            </div>
        <div class="row">
                <div class="col-lg-12">
                   
                        <div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-hover dataTables">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th class="text-center">Tel</th>
                                        <th class="text-center">E-mail</th>
                                        <th>Adresse</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${clients}" var="client">
                                <c:url value="/client/details/${client.getIdClient() }" var="urlclient"/>
                                 <tr class="odd gradeX"  data-href="${urlclient }" style="cursor: pointer;">
                                        <td class="clickable-row" data-href="${urlclient }" style="cursor: pointer;"><b>${client.getGnr() }.&nbsp;${client.getPrenom() }&nbsp;${client.getNom() }</b></td>
                                        <td class="text-center clickable-row" data-href="${urlclient }" style="cursor: pointer;"><b>${client.getTel() }</b></td>
                                        <td class="text-center clickable-row" data-href="${urlclient }" style="cursor: pointer;"><b>${client.getMail() }</b></td>
                                        <td class="clickable-row" data-href="${urlclient }" style="cursor: pointer;">${client.getAdresse() } &nbsp;${client.getAdresse2() }&nbsp;${client.getVille() } </td>
                                        <td class="text-center">
                                        <c:url value="/client/modifier/${client.getIdClient() }" var="urlModifier"/>
										<a href="${urlModifier }" class="btn btn-primary btn-circle"><i class="fa  fa-edit"></i></a>
										&nbsp;|&nbsp;
			                            <button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#modal_${client.getIdClient() }"><i class="fa fa-trash-o "></i></button>
			                            
			                            <!-- Modal -->
			                            <div class="modal fade" id="modal_${client.getIdClient() }" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                                <div class="modal-dialog">
			                                    <div class="modal-content">
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                            <h4 class="modal-title" id="myModalLabel">CONFIRMATION!</h4>
			                                        </div>
			                                        <div class="modal-body">
			                                        <div class="row">
			                                        <div class="col-xs-2">
			                                        <i class="fa fa-warning fa-4x "></i>
						                            </div>
						                            <div class="col-xs-10">
						                            Voulez-vous vraiment supprimer le client : <br>
						                            <img src="<c:url value="/files/${client.getPhoto() }" />" width="50px" height="50px"/>&nbsp;<b>${client.getNom() }&nbsp;${client.getPrenom() }</b>					                            
 						                            </div>
			                                        </div>
			                                         </div>
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			                                            <c:url value="/client/supprimer/${client.getIdClient() }" var="urlSupprimer"/>
			                                            <a href="${urlSupprimer }" class="btn btn-danger"><i class="fa fa-trash-o "></i> Spprimer!</a>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
										</td>
                                    </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>

        </div>
    </div>
     <script >
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
    </script>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   