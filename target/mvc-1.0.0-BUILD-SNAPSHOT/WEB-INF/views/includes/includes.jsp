<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@ page session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page trimDirectiveWhitespaces="true" %>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Fondation Mohamed 6 des Oulema Africans</title>
    
    <link rel="icon" type="image/png" href="<%=request.getContextPath() %>/resources/launcher.png" sizes="32x32">

    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath() %>/resources/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="<%=request.getContextPath() %>/resources/vendor/bootstrap/css/datepicker.css" rel="stylesheet">
	<link href="<%=request.getContextPath() %>/resources/vendor/bootstrap/css/planning.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<%=request.getContextPath() %>/resources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=request.getContextPath() %>/resources/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=request.getContextPath() %>/resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  	 <!-- DataTables CSS -->
    <link href="<%=request.getContextPath() %>/resources/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<%=request.getContextPath() %>/resources/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
      
    <!-- jQuery -->
    <script src="<%=request.getContextPath() %>/resources/fullcalendar/node_modules/jquery/dist/jquery.js"></script>
    	<script src="<%=request.getContextPath() %>/resources/js/formApp.js"></script>
    <!-- Full Calendar -->
    
	    <link href='<%=request.getContextPath() %>/resources/fullcalendar/dist/fullcalendar.css' rel='stylesheet' />
		<script src='<%=request.getContextPath() %>/resources/fullcalendar/node_modules/moment/moment.js'></script>
		

</head>