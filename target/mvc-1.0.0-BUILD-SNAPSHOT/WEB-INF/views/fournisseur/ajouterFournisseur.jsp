<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Fournisseurs</h1>
                </div>
            </div>
            
        <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        <c:if test="${fournisseur.getIdFournisseur()!=null }">
                        Modification Fournisseur
                        </c:if>
                        <c:if test="${fournisseur.getIdFournisseur()==null }">
                        Nauveau Fournisseur
                        </c:if>
                            
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        
                            <c:url value="/fournisseur/enregistrer" var ="urlEnregistrer" />
							<f:form modelAttribute="fournisseur" action="${urlEnregistrer }" enctype="multipart/form-data" role = "form">
							<f:hidden path="idFournisseur"/>
							<f:hidden path="photo"/>
						  <div class="form-row">
						  <div class="form-group col-md-2">
						     <label>Civilit�</label>
                             <f:select path="gnr" class="form-control">
                             	<option value="Ste">Ste.</option>
                             	<option value="M">M.</option>
                                <option value="Mme">Mme.</option>
                                <option value="Mlle">Mlle.</option>
                                <option value="Dr">Dr.</option>
                             </f:select>
						    </div>
						    <div class="form-group col-md-5">
						     <label>Pr�nom</label>
                             <f:input path="prenom" class="form-control" placeholder="Pr�nom" />
						    </div>
						    <div class="form-group col-md-4">
						      <label>Nom</label>
                             <f:input path="nom" class="form-control" placeholder="Nom" />
						    </div>
						  </div>
						  <div class="form-group form-row col-md-11">
						    <label>Telphone</label>
                             <f:input path="tel" class="form-control" placeholder="Telphone" />
						  </div>
						  <div class="form-group form-row col-md-11">
						    <label>E-mail</label>
                             <f:input path="mail" class="form-control" placeholder="E-mail" />
						  </div>
						  <div class="form-group form-row col-md-11">
						    <label>Adresse</label>
                             <f:input path="adresse" class="form-control" placeholder="Rue 1 .." />
						  </div>
						  <div class="form-group form-row col-md-11">
						    <label>Adresse 2</label>
                             <f:input path="adresse2" class="form-control" placeholder="Rue 2 .." />
						  </div>
						  <div class="form-row">
						    <div class="form-group col-md-11">
						      <label>Ville</label>
                             <f:input path="ville" class="form-control" placeholder="Ville" />
						    </div>
						  </div>
						  <div class="form-groupform-row col-md-11">
                             <label>Photo</label>
                            <input type="file" name="file" class="filestyle" data-btnClass="btn-primary">
                         </div>	
                            
                        </div>
                        <div class="panel-footer">
                        <div class="row">
                        <div  class="col-md-3"></div>
                        <div  class="col-md-6">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Enregister</button>
                            <a href="<c:url value="/fournisseur/" />" class="btn btn-danger"> <i class="fa fa-arrow-left"></i> Annuler</a>
                        </div>
                        <div  class="col-md-3"></div>
                        </div>
                        </div>
                        </f:form>
                    </div>
                </div>
            </div>

        </div>

    </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   