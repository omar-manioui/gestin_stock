<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                        </li>
                        <c:url value="/home/" var="home" />
                        <li>
                            <a href="${home }"><i class="fa fa-dashboard fa-fw"></i> Tableau De Bord</a>
                        </li>
                        <c:url value="/client/" var="clientURL" />
                        <li>
                            <a href="${clientURL }"><i class="fa fa-users fa-fw"></i> Agents</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-usd fa-fw"></i> Consommations</a>
                            <ul class="nav nav-second-level">
                             <c:url value="/commande_client/" var="commande_clientURL" />
		                        <li>
		                            <a href="${commande_clientURL }"><i class="fa fa-list-alt fa-fw"></i> Consommations</a>
		                        </li> 													
                            </ul>
                        </li>
                         <c:url value="/fournisseur/" var="fournisseurURL" />
                        <li>
                            <a href="${fournisseurURL }"><i class="fa fa-users fa-fw"></i> Fournisseurs</a>
                        </li>
                        <c:url value="/commande_fournisseur/" var="commande_fournisseurURL" />
                        <li>
                            <a href="${commande_fournisseurURL }"><i class="fa fa-list-alt fa-fw"></i> Commandes</a>
                        </li>
                        <c:url value="/article/" var="articleURL" />
                        <li>
                            <a href="${articleURL }"><i class="fa fa-archive fa-fw"></i> Articles</a>
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-th fa-fw"></i> Categories</a>
                            <ul class="nav nav-second-level">
                            <c:forEach items="${categories}" var="categorie">
                             <c:url value="/article/category/${categorie.getIdCategory() }" var="categorieUrl" />
		                        <li>
		                            <a href="${categorieUrl }"><i class="fa fa-th-large fa-fw"></i> ${categorie.getCode() }</a>
		                        </li>
                            </c:forEach>  													
                            </ul>
                        </li>    
                        
                        <li>
                            <a href="#"><i class="fa fa-building-o fa-fw"></i> Entrepots</a>
                            <ul class="nav nav-second-level">
                            <c:forEach items="${entropots}" var="entropot">
                             <c:url value="/article/entropot/${entropot.getIdEntropot() }" var="entropotUrl" />
		                        <li>
		                            <a href="${entropotUrl }"><i class="fa  fa-check  fa-fw"></i> ${entropot.getCode() }</a>
		                        </li>
                            </c:forEach>  													
                            </ul>
                        </li>    
                        
                        
                        <li>
                            <a href="#"><i class="fa fa-anchor fa-fw"></i> Propri�t�s de la Fondation</a>
                            <ul class="nav nav-second-level">
                             <c:url value="/prop/" var="propUrl" />
		                        <li>
		                            <a href="${propUrl }"><i class="fa fa-cubes fa-fw"></i> Propri�t�s de la Fondation</a>
		                        </li>
		                        <c:url value="/prop/" var="propUrl" />
		                        <li>
		                            <a href="${propUrl }"><i class="fa fa-wrench  fa-fw"></i> Maintenance</a>
		                        </li>		
		                        <c:url value="/prop/keys" var="keysUrl" />
		                        <li>
		                            <a href="${keysUrl }"><i class="fa fa-key  fa-fw"></i> Cl�s</a>
		                        </li>											
                            </ul>
                        </li>                  

                        <li class ="active">
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Configuration<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <c:url value="/categorie/" var="categorieURL" />
		                        <li>
		                            <a href="${categorieURL }"><i class="fa fa-th fa-fw"></i> Categories</a>
		                        </li>
		                        <c:url value="/entropot/" var="entropotURL" />
		                        <li>
		                            <a href="${entropotURL }"><i class="fa fa-building-o fa-fw"></i> Entrepots</a>
		                        </li>
	                              													
                            </ul>
                        </li>
                        
                        																		
                    </ul>
                </div>
            </div>
      </nav>      
            
 