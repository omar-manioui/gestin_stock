<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tableau De Bord</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
               
               <div class="row">
                <div class="col-lg-12">
                <c:if test="${stckAlertes.size()>0 }">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-warning fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                               
                                    <div class="huge">${stckAlertes.size() }</div>
                                    <div>Alertes de Stock!</div>
                                </div>
                            </div>
                        </div>
                        <a href="${alertesURL }">
                            <div class="panel-footer">
                                <span class="pull-left">Voir les d�tails</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                    </c:if>
                    </div>
                    </div>
                    
                <div class="row">
                <div class="col-lg-7">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Planning
                        </div>
                        <div class="panel-body">
                        <div id="FullCalendar"></div> 
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="${keysUrl }"><i class="fa fa-users fa-fw"></i> Cl�s</a>
                        </div>
                        <div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-hover dataTables1">
                                <thead>
                                    <tr>
                                        <th>N�</th>
                                        <th class="text-center">Nom</th>
                                        <th class="text-center">Reste</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${keys}" var="key">
                                <c:url value="/prop/keydetails/${key.idCle }" var="urlKey"/>
                                 <tr class="odd gradeX clickable-row" data-href="${urlKey }" style="cursor: pointer;">
                                        <td>${key.idCle }</td>
                                        <td class="text-center">${key.code}</td>
                                        <td class="text-center">${key.reste }</td>
                                        
                                </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </div>
                
                <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="${clientURL }"><i class="fa fa-users fa-fw"></i> Agents</a>
                        </div>
                        <div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-hover dataTables1">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th class="text-center">Tel</th>
                                        <th class="text-center">E-mail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${clients}" var="client">
                                <c:url value="/client/details/${client.getIdClient() }" var="urlclient"/>
                                 <tr class="odd gradeX clickable-row" data-href="${urlclient }" style="cursor: pointer;">
                                        <td>${client.getGnr() }.&nbsp;${client.getPrenom() }&nbsp;${client.getNom() }</td>
                                        <td class="text-center">${client.getTel() }</td>
                                        <td class="text-center">${client.getMail() }</td>
                                        
                                </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="${fournisseurURL }"><i class="fa fa-users fa-fw"></i> Fournisseurs</a>
                        </div>
                        <div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-hover dataTables1">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th class="text-center">Tel</th>
                                        <th class="text-center">E-mail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${fournisseurs}" var="fournisseur">
                                <c:url value="/fournisseur/details/${fournisseur.getIdFournisseur() }" var="urlfournisseur"/>
                                 <tr class="odd gradeX clickable-row" data-href="${urlfournisseur }" style="cursor: pointer;" data-href="${urlclient }" style="cursor: pointer;">
                                        <td>${fournisseur.getGnr() }.&nbsp;${fournisseur.getPrenom() }&nbsp;${fournisseur.getNom() }</td>
                                        <td class="text-center">${fournisseur.getTel() }</td>
                                        <td class="text-center">${fournisseur.getMail() }</td>
                                        
                                </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div></div>



        </div>
        <!-- /#page-wrapper -->

    </div>
    <script src='<%=request.getContextPath() %>/resources/fullcalendar/dist/fullcalendar.js'></script>
    <script src='<%=request.getContextPath() %>/resources/fullcalendar/dist/locale/fr.js'></script>
    <script>
    $(document).ready(function() {
        $('#FullCalendar').fullCalendar({
			locale: 'fr',
        	header: {
				left: 'title',
				center: 'month,listMonth',
				right: 'prev,next '
			},
            theme: 'bootstrap3',
            editable: false,
            events: "/mvc/calendar"
        }); });
    
    </script>
     <script >
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
    </script>
    <script>
    var dEvent = $('.event');

 // var h = dEvent.attr('data-duration');
 // var pos = dEvent.attr('data-start');



 $('.event').each(function() {
   var $this = $(this);
   var h = $this.attr('data-duration');
   var pos = $this.attr('data-start');
   $this.css({
     'height' : h * 60,
     'top' : pos * 60
   });
 });

 $(window).resize(function() {
   if ($(window).width() < '650') {
     $('.event-column').css('margin-left', "60px");
   } else {
     $('.event-column').css('margin-left', "0");
     $('.event-column').css('left', '0')
   }
 });


   if ($(window).width() < '650') {
     $('.event-column').css('margin-left', "60px");
   }

 $("#goRight").click(function(event) {
   event.preventDefault();
   var left = $('.event-column').css('left');
   var leftVal = parseInt(left.replace("px", ""));
   if( leftVal === 0 ) {
     $('.event-column').css('left', "-114px");
   } else if (leftVal > '-570') {
     var newPos = (leftVal - 114);
     $('.event-column').css({
       left : newPos + 'px'
     });
    }
   $("#goLeft").css("opacity", "1");
 });

 $("#goLeft").click(function(event) {
   event.preventDefault();
   
   var left = $('.event-column').css('left');
   var leftVal = parseInt(left.replace("px", ""));
   if( leftVal === 0 ) {
     event.preventDefault();
   } else if (leftVal >= '-684' ) {
     var newPos = (leftVal + 114);
     $('.event-column').css({
       left : newPos + 'px'
     });
    }
   $("#goLeft").css("opacity", "1");
 });



    </script>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   