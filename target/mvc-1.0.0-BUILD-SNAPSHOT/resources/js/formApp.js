
$(document).ready(function () {
	  //called when key is pressed in textbox
	  $(".numero").keypress(function (e) {
	     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	    	 		if(e.which == 45 || e.which == 46){return true;}
	               return false;
	    }
	   });
	});


$(document).ready(function() {
	$("#prixUnitaireHT").on("keyup", function() {
		tvaKeyUpFunction();
	});
	$("#tauxTva").on("keyup", function() {
		tvaKeyUpFunction();
	});
	$("#prixUnitaireTTC").on("keyup", function() {
		tvaKeyUpFunction1();
	});
});

tvaKeyUpFunction = function() {
	var prixUnitHT = $("#prixUnitaireHT").val();
	var tauxTva = $("#tauxTva").val();
	var prixUnitTTC = parseFloat(parseFloat(prixUnitHT) * parseFloat(tauxTva) / 100 + parseFloat(prixUnitHT));
	$("#prixUnitaireTTC").val(prixUnitTTC);
}
tvaKeyUpFunction1 = function() {
	var prixUnitTTC = $("#prixUnitaireTTC").val();
	var tauxTva = $("#tauxTva").val();
	var prixUnitHT = parseFloat(100 * parseFloat(prixUnitTTC) / (100 + parseFloat(tauxTva)));
	$("#prixUnitaireHT").val(prixUnitHT);
}
