<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Categories</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
	            <div class="col-lg-12">
	            <ol class="breadcrumb">
                	<li><a href="<c:url value="/categorie/nouveau" />" ><i class="fa fa-plus  "></i> Ajouter</a></li>
                </ol>	            
	            </div>
            
            </div>
        <div class="row">
                            <table width="100%" class="table table-striped table-hover dataTables">
                                <thead>
                                    <tr>
                                    	<th>#</th>
                                        <th>Nom Categorie</th>
                                        <th>Designation</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${categories}" var="categorie">
                                 <tr class="odd gradeX">
                                        <td>${categorie.getIdCategory() }</td>
                                        <td>${categorie.getCode() }</td>
                                        <td>${categorie.getDesignation() }</td>
                                        <td class="text-center ">
                                        <c:url value="/categorie/modifier/${categorie.getIdCategory() }" var="urlModifier"/>
										<a href="${urlModifier }" class="btn btn-primary btn-circle"><i class="fa  fa-edit"></i></a>
										&nbsp;|&nbsp;
			                            <button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#modal_${categorie.getIdCategory() }"><i class="fa fa-trash-o "></i></button>
			                            
			                            <!-- Modal -->
			                            <div class="modal fade" id="modal_${categorie.getIdCategory() }" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                                <div class="modal-dialog">
			                                    <div class="modal-content">
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                            <h4 class="modal-title" id="myModalLabel">CONFIRMATION!</h4>
			                                        </div>
			                                        <div class="modal-body">
			                                        <div class="panel panel-red">
									                        <div class="panel-heading">
									                            <div class="row">
									                                <div class="col-xs-3">
									                                    <i class="fa fa-warning fa-5x"></i>
									                                </div>
									                                <div class="col-xs-9 ">
									                               	<h3>Voulez-vous vraiment supprimer la Categorie :&nbsp;<b>${categorie.getCode() }</b></h3>				                            

									                                </div>
									                            </div>
									                        </div>
									                    </div>
			                                         </div>
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			                                            <c:url value="/categorie/supprimer/${categorie.getIdCategory() }" var="urlSupprimer"/>
			                                            <a href="${urlSupprimer }" class="btn btn-danger"><i class="fa fa-trash-o "></i> Spprimer!</a>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
										</td>
                                    </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
            </div>

        </div>
    </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   