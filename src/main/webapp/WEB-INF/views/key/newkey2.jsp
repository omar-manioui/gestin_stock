<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Cl�s</h1>
                </div>
            </div>
            
            <div class="row alert alert-danger alert-dismissable center">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Impossible! La Quantit� des cl�s retante en stock est inf�rieur � la quantit� entr�e !
          </div>
            
        <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        <c:if test="${cle.getIdCle()!=null }">
                        Modification Cl�
                        </c:if>
                        <c:if test="${cle.getIdCle()==null }">
                        Nauveau Cl�
                        </c:if>
                            
                        </div>
                        <div class="panel-body">
                        
                            <c:url value="/keys/savekey" var ="urlSave" />
							<f:form modelAttribute="cle" action="${urlSave }" role="form">
							<f:hidden path="idCle"/>
							<f:hidden path="reste"/>
						    <div class="form-group form-row col-md-10">
						     <label>Nom de Cl�</label>
                             <f:input path="code" class="form-control" placeholder="Nom de Cl�" />
                             
						    </div>
						    <div class="form-group form-row col-md-10">
						      <label>Designation</label>
                             <f:input path="designation" class="form-control" placeholder="Designation" />
						    </div>
						    
							<div class="form-group form-row col-md-10">
                                <label>Quantit�</label>
                                <f:input path="qte" class="form-control numero" placeholder="Quqntit�" />
                            	</div>
                            </div>
                        <div class="panel-footer">
                        <div class="row">
                        <div  class="col-md-3"></div>
                        <div  class="col-md-6">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Enregister</button>
                            <a href="<c:url value="/keys/" />" class="btn btn-danger"> <i class="fa fa-arrow-left"></i> Annuler</a>
                        </div>
                        <div  class="col-md-3"></div>
                        </div>
                        </div>
                        </f:form>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>

    </div>
    <!-- /#wrapper -->
    
 
 
 <script type="text/javascript" src="<%=request.getContextPath() %>/resources/bootstrap-filestyle.min.js"></script>
 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   