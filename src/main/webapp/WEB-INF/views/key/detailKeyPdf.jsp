<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body id="page-wrapper">


<div class="col-lg-12">
         <div class="row">
                <div class="page-headerpdf col-lg-12">
                    <h1 style="padding-top: 10px;"><c:out value="${cle.getCode()}"/></h1>
                </div>
            </div>

        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">

                                <li class="list-group-item">
                                    <label>Designation</label>${cle.designation}
                                </li>
                                <li class="list-group-item">
                                    <label>Quqntit�</label>${cle.qte }
                                </li>
                                <li class="list-group-item">
                                    <label>Reste</label>${cle.reste }
                                </li>
                            </ul>
                      </div>            
            </div>
             <div class="row">
                <div class="col-lg-12">
                    <h2 style="border-bottom: 1px solid #eee; color: #337ab7;">Re�u par :</h2>
                </div>
            </div>
            <table width="100%" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>N�</th>
                                        <th>Agent</th>
                                        <th>Commentaire</th>
                                        <th>Quantite</th>
                                    	<th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${cmdkeys}" var="cmdkey">
                                 <tr class="info" style="page-break-inside: avoid;">
                                 		<td>${cmdkey.idCmdKey }</td>
                                        <td><b>${cmdkey.getClient().getGnr() }.&nbsp;${cmdkey.getClient().getPrenom() }&nbsp;${cmdkey.getClient().getNom() }</b></td>
                                        <td><b>${cmdkey.getCommentaire() }</b></td>
                                        <td>${cmdkey.getQte() }</td>
                                    	<td><fmt:formatDate value="${cmdkey.getDate()}" pattern="dd/MM/yyyy" /></td>
                                    	
                                    </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
            

        </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   