<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
        <div class="row">
                <div class="page-header col-lg-12">
                <div class="col-lg-6">
                    <h1><c:out value="${cle.getCode()}"/></h1>
                </div>
                <div class="col-lg-6 text-right">
                    <c:url value="/keys/editkey/${cle.getIdCle() }" var="urlModifier"/>
					<a href="${urlModifier }" class="btn btn-primary"><i class="fa  fa-edit"></i> Modifier</a>
					</div>
                </div>
            </div>

        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">

                                <li class="list-group-item">
                                    <label>Designation</label>${cle.getDesignation()}
                                </li>
                                <li class="list-group-item">
                                    <label>Quantit�</label>${cle.getQte() }
                                </li>
                            </ul>
                      </div>     
                      <div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">
                                <li class="list-group-item ">
                                    <label>Reste</label>${cle.getReste() }
                                </li>
                                
                            </ul>
                      </div>            
            </div>
            
            <ol class="breadcrumb text-right">
                	<li><a target="_blank" href="<c:url value="/keys/detailspdf/${cle.getIdCle() }" />" ><i class="fa  fa-print   "></i> Imprimer</a></li>
                </ol> 
             <div class="row">
                <div class="col-lg-12">
                    <h2 style="border-bottom: 1px solid #eee; color: #337ab7;">Re�u par :</h2>
                </div>
            </div>
            <table width="100%" class="table table-striped table-hover dataTables">
                                <thead>
                                    <tr>
                                    	<th>N�</th>
                                        <th>Agent</th>
                                        <th>Commentaire</th>
                                        <th>Quantite</th>
                                    	<th>Date</th>
                                    	<th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${cmdkeys}" var="cmdkey">
                                 <tr class="info">
                                 		<td>${cmdkey.idCmdKey }</td>
                                        <td><b>${cmdkey.getClient().getGnr() }.&nbsp;${cmdkey.getClient().getPrenom() }&nbsp;${cmdkey.getClient().getNom() }</b></td>
                                        <td><b>${cmdkey.getCommentaire() }</b></td>
                                        <td>${cmdkey.getQte() }</td>
                                    	<td><fmt:formatDate value="${cmdkey.getDate()}" pattern="dd/MM/yyyy" /></td>
                                    	<td>
                                 		
                                 		<button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#modal1_${cmdkey.idCmdKey}"><i class="fa fa-trash-o "></i></button>
                                 		 <!-- Modal -->
			                            <div class="modal fade" id="modal1_${cmdkey.idCmdKey}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                                <div class="modal-dialog">
			                                    <div class="modal-content">
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                            <h4 class="modal-title" id="myModalLabel">CONFIRMATION!</h4>
			                                        </div>
			                                        <div class="modal-body">
			                                        <div class="panel panel-red">
									                        <div class="panel-heading">
									                            <div class="row">
									                                <div class="col-xs-3">
									                                    <i class="fa fa-warning fa-5x"></i>
									                                </div>
									                                <div class="col-xs-9 ">
									                               	<h3>Voulez-vous vraiment supprimer la Cl� N�:&nbsp;<b>${cmdkey.idCmdKey}</b></h3>				                            

									                                </div>
									                            </div>
									                        </div>
									                    </div>
			                                         </div>
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			                                            <c:url value="/keys/deletcmdkey/${cmdkey.idCmdKey }" var="urlSuppr"/>
			                                            <a href="${urlSuppr }" class="btn btn-danger"><i class="fa fa-trash-o "></i> Spprimer!</a>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
                                 		</td>
                                    </tr>
                                </c:forEach>
                                    
                                </tbody>
                                <tfoot>
                                <th>
                                <c:url value="/keys/addcmdkey/${cle.getIdCle() }" var="urlAddCmdKey"/>
								<a href="${urlAddCmdKey }" class="btn btn-info"><i class="fa  fa-plus"></i> Ajouter</a>
								</th>
                                </tfoot>
                            </table>
            

        </div>
    </div>
    <!-- /#wrapper -->
    

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   