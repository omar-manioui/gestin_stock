<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body id="page-wrapper">


<div class="col-lg-12">
         
        <div class="row">
                <div class="page-headerpdf col-lg-12">
                    <h1 style="padding-top: 10px;"><c:out value="${fournisseur.getGnr() }. ${fournisseur.getPrenom()} ${fournisseur.getNom() }"/></h1>
                </div>
            </div>

        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">

                                <li class="list-group-item">
                                <i class="icon fa fa-phone"></i>
                                    <label>Telephone </label>${fournisseur.getTel()}
                                </li>
                                <li class="list-group-item"> 
                                <i class="icon fa fa-envelope-o"></i>
                                    <label>E-mail</label>${fournisseur.getMail() }
                                </li>
                            
                                <li class="list-group-item ">
                                <i class="icon fa fa-map-marker"></i>
                                    <label>Adresse</label>${fournisseur.getAdresse() }&nbsp;${fournisseur.getAdresse2() } 
                                </li>
                                <li class="list-group-item">
                                 <i class="icon fa fa-map-marker"></i>
                                    <label>Ville</label>${fournisseur.getVille() }
                                </li>
                                
                            </ul>
                      </div>            
            </div>
             <div class="row">
                <div class="col-lg-12">
                    <h2 style="border-bottom: 1px solid #eee; color: #337ab7;">Articles livr�s</h2>
                </div>
            </div>
            <table width="100%" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Article</th>
                                        <th>Commentaire</th>
                                        <th>Quantite</th>
                                    	<th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${lcmdarticles}" var="lcmdarticle">
                                 <tr style="page-break-inside: avoid;">
                                        <td><b>${lcmdarticle.getArticle().getCodeArticle() }</b></td>
                                        <td><b>${lcmdarticle.getCommentaire() }</b></td>
                                        <td>${lcmdarticle.getQte() }</td>
                                 		<td><fmt:formatDate value="${lcmdarticle.getCommandeFournisseur().getDateCommande()}" pattern="dd/MM/yyyy" />
                                 		</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
            

        </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   