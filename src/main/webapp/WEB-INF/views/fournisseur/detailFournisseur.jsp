<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
         <div class="row">
        		<div class="col-lg-2"><img src="<%=request.getContextPath() %>/images/fournisseur/${fournisseur.getPhoto() }" width="100px" height="130px" style="margin: 10px;"/>
        		</div>
                 <div class="page-header col-lg-10">
                <div class="col-lg-6">
                    <h1 ><c:out value="${fournisseur.getGnr() }. ${fournisseur.getPrenom()} ${fournisseur.getNom() }"/></h1>
                </div>
                    <div class="col-lg-6 text-right">
                    <c:url value="/fournisseur/modifier/${fournisseur.getIdFournisseur() }" var="urlModifier"/>
					<a href="${urlModifier }" class="btn btn-primary"><i class="fa  fa-edit"></i> Modifier</a>
					</div>
                </div>
            </div>

        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">

                                <li class="list-group-item">
                                <i class="icon fa fa-phone"></i>
                                    <label>Telephone </label>${fournisseur.getTel()}
                                </li>
                                <li class="list-group-item"> 
                                <i class="icon fa fa-envelope-o"></i>
                                    <label>E-mail</label>${fournisseur.getMail() }
                                </li>
                            </ul>
                      </div>     
                      <div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">
                                <li class="list-group-item ">
                                <i class="icon fa fa-map-marker"></i>
                                    <label>Adresse</label>${fournisseur.getAdresse() }&nbsp;${fournisseur.getAdresse2() } 
                                </li>
                                <li class="list-group-item">
                                 <i class="icon fa fa-map-marker"></i>
                                    <label>Ville</label>${fournisseur.getVille() }
                                </li>
                                
                            </ul>
                      </div>            
            </div>
            <ol class="breadcrumb text-right">
                	<li><a target="_blank" href="<c:url value="/fournisseur/detailspdf/${fournisseur.getIdFournisseur() }" />" ><i class="fa  fa-print   "></i> Imprimer</a></li>
                </ol> 
             <div class="row">
                <div class="col-lg-12">
                    <h2 style="border-bottom: 1px solid #eee; color: #337ab7;">Articles livr�s</h2>
                </div>
            </div>
            <table width="100%" class="table table-striped table-hover dataTables">
                                <thead>
                                    <tr>
                                        <th>Article</th>
                                        <th>Commentaire</th>
                                        <th>Quantite</th>
                                    	<th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${lcmdarticles}" var="lcmdarticle">
                                 <tr class="info">
                                        <td><b>${lcmdarticle.getArticle().getCodeArticle() }</b></td>
                                        <td><b>${lcmdarticle.getCommentaire() }</b></td>
                                        <td>${lcmdarticle.getQte() }</td>
                                 		<td><fmt:formatDate value="${lcmdarticle.getCommandeFournisseur().getDateCommande()}" pattern="dd/MM/yyyy" />
                                 		</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
            

        </div>
    </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   