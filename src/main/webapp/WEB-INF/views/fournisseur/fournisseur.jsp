<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Fournisseurs</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
	            <div class="col-lg-12">
	            <ol class="breadcrumb">
                	<li><a href="<c:url value="/fournisseur/nouveau" />" ><i class="fa fa-plus  "></i> Ajouter</a></li>
                </ol>	            
	            </div>
            
            </div>
        <div class="row">
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-hover dataTables">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th class="text-center">Tel</th>
                                        <th class="text-center">E-mail</th>
                                        <th>Adresse</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${fournisseurs}" var="fournisseur">
                                <c:url value="/fournisseur/details/${fournisseur.getIdFournisseur() }" var="urlFournisseur"/>
                                 <tr class="odd gradeX">
                                        <td class="clickable-row" data-href="${urlFournisseur }" style="cursor: pointer;"><b>${fournisseur.getGnr() }.&nbsp;${fournisseur.getPrenom() }&nbsp;${fournisseur.getNom() }</b></td>
                                        <td class="text-center clickable-row" data-href="${urlFournisseur }" style="cursor: pointer;">${fournisseur.getTel() }</td>
                                        <td class="text-center clickable-row" data-href="${urlFournisseur }" style="cursor: pointer;">${fournisseur.getMail() }</td>
                                        <td class="clickable-row" data-href="${urlFournisseur }" style="cursor: pointer;">${fournisseur.getAdresse() } &nbsp;${fournisseur.getAdresse2() }&nbsp;${fournisseur.getVille() }</td>
                                        <td class="text-center">
                                        <c:url value="/fournisseur/modifier/${fournisseur.getIdFournisseur() }" var="urlModifier"/>
										<a href="${urlModifier }" class="btn btn-primary btn-circle"><i class="fa  fa-edit"></i></a>
										&nbsp;|&nbsp;
			                            <button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#modal_${fournisseur.getIdFournisseur() }"><i class="fa fa-trash-o "></i></button>
			                            
			                            <!-- Modal -->
			                            <div class="modal fade" id="modal_${fournisseur.getIdFournisseur() }" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                                <div class="modal-dialog">
			                                    <div class="modal-content">
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                            <h4 class="modal-title" id="myModalLabel">CONFIRMATION!</h4>
			                                        </div>
			                                        <div class="modal-body">
			                                        <div class="panel panel-red">
									                        <div class="panel-heading">
									                            <div class="row">
									                                <div class="col-xs-3">
									                                    <i class="fa fa-warning fa-5x"></i>
									                                </div>
									                                <div class="col-xs-9 ">
									                               	<h3>Voulez-vous vraiment supprimer le Fournisseur :&nbsp;<b>${fournisseur.getGnr() }.&nbsp;${fournisseur.getPrenom() }&nbsp;${fournisseur.getNom() }</b></h3>				                            

									                                </div>
									                            </div>
									                        </div>
									                    </div>
			                                         </div>
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			                                            <c:url value="/fournisseur/supprimer/${fournisseur.getIdFournisseur() }" var="urlSupprimer"/>
			                                            <a href="${urlSupprimer }" class="btn btn-danger"><i class="fa fa-trash-o "></i> Spprimer!</a>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
										</td>
                                    </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                        </div>
            </div>
        </div>
    </div>
 <script >
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
    </script>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   