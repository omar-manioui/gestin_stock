<%@ include file="/WEB-INF/views/includes/includes.jsp" %>


<body>

    
    		<div class="alert alert-danger alert-dismissable center">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Login ou mot de passe incorrecte!.
          </div>

    <div class="container">

    <div class="row col-md-4 col-md-offset-4 logo-panel">
    <img alt="" src="<%=request.getContextPath() %>/resources/logo.png" class="img-responsive">
    </div>
        
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Veuillez vous Connecter</h3>
                    </div>
                    <div class="panel-body">
                        <form action="<c:url value="/j_spring_security_check"></c:url>" method="post" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="j_username" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="j_password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember-me" type="checkbox" >Se souvenir de moi
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="Login" />
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<%@ include file="/WEB-INF/views/includes/js.jsp" %>    
