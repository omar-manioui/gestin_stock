<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Propri�t�s de la Fondation</h1>
                </div>
            </div>
            
        <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        <c:if test="${cmdprop.idCmdProp !=null }">
                        Modification 
                        </c:if>
                        <c:if test="${cmdprop.idCmdProp ==null }">
                        Nauveau
                        </c:if>
                            
                        </div>
                        <c:url value="/prop/savecmdprop" var ="urlSavec" />
							<f:form modelAttribute="cmdprop" method="POST" action="${urlSavec }" enctype="multipart/form-data" role="form">
                        <div class="panel-body">
                        
                            
							<f:hidden path="idCmdProp"/>
							<f:hidden path="prop.idProp"/>
						    <div class="form-group form-row col-md-10">
						     <label>Nom de Propri�t�</label>
                             <f:input path="prop.codeProp" class="form-control" placeholder="Nom de Propri�t�" readonly="true"/>
						    </div>
						    <div class="form-group form-row col-md-10">
							<label>Agent</label>
                   			<f:select class="form-control" path="client.idClient">
                   				 <c:forEach items="${clients}" var="client">
                   				 <f:option value="${client.idClient}" label="${client.getNom()} ${client.getPrenom()}"/>
                   				 </c:forEach>
                   			</f:select>
							</div>
						    <div class="form-group form-row col-md-10">
						      <label>Commentaire</label>
						      
                             <f:input path="commentaire" class="form-control" placeholder="Commentaire" />
						    </div>
						    
							<div class="form-group form-row col-md-10">
                                <label>Quantit�</label>
                                <f:input path="qte" class="form-control numero" placeholder="Quqntit�" />
                            	</div>
                            	<div class="form-group form-row col-md-10">
                               <label>Date</label>
                                <div class="input-group date datepicker" data-date-format="mm/dd/yyyy">
				                    <f:input path="date" type='text' class="form-control" readonly="true"/>
				                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>
                           </div>
                            </div>
                            
                            
                        <div class="panel-footer">
                        <div class="row">
                        <div  class="col-md-3"></div>
                        <div  class="col-md-6">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Enregister</button>
                            <a href="<c:url value="/prop/details/${cmdprop.prop.idProp}" />" class="btn btn-danger"> <i class="fa fa-arrow-left"></i> Annuler</a>
                        </div>
                        <div  class="col-md-3"></div>
                        </div>
                        </div>
                        </f:form>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>

    </div>
    <!-- /#wrapper -->
    
 
 
 <script type="text/javascript" src="<%=request.getContextPath() %>/resources/bootstrap-filestyle.min.js"></script>
 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   