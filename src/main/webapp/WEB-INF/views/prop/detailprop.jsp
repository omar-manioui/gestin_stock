<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
        <div class="row">
                <div class="page-header col-lg-12">
                <div class="col-lg-6">
                    <h1><c:out value="${prop.codeProp }"/></h1>
                </div>
                <div class="col-lg-6 text-right">
                    <c:url value="/prop/modifier/${prop.idProp}" var="urlModifier"/>
					<a href="${urlModifier }" class="btn btn-primary"><i class="fa  fa-edit"></i> Modifier</a>
					</div>
                </div>
            </div>

        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">

                                <li class="list-group-item">
                                    <label>Designation</label>${prop.designation}
                                </li>
                                <li class="list-group-item">
                                    <label>Categorie</label>${prop.getCategory().getCode() }
                                </li>
                                <li class="list-group-item">
                                    <label>Entropot</label>${prop.getEntropot().getCode() }
                                </li>
                                <li class="list-group-item ">
                                    <label>Date d'Entr�e</label><fmt:formatDate value="${prop.getDate()}" pattern="dd/MM/yyyy" />
                                </li>
                            </ul>
                      </div>     
                      <div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">
                                
                                <li class="list-group-item">
                                    <label>Quqntit�</label>${prop.qte }
                                </li>
                                <li class="list-group-item">
                                    <label>Reste</label>${prop.reste }
                                </li>
                                <li class="list-group-item">
                                    <label>Valeur</label>${prop.valeur }<b> DH</b>
                                </li>
                                
                            </ul>
                      </div>            
            </div>
            
            <ol class="breadcrumb text-right">
                	<li><a target="_blank" href="<c:url value="/prop/detailspdf/${prop.idProp}" />" ><i class="fa  fa-print   "></i> Imprimer</a></li>
                </ol> 
             <div class="row">
                <div class="col-lg-12">
                    <h2 style="border-bottom: 1px solid #eee; color: #337ab7;">Re�u par :</h2>
                </div>
            </div>
            <table width="100%" class="table table-striped table-hover dataTables">
                                <thead>
                                    <tr>
                                    	<th>N�</th>
                                        <th>Agent</th>
                                        <th>Commentaire</th>
                                        <th>Quantite</th>
                                    	<th>Date</th>
                                    	<th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${cmdprops}" var="cmdprop">
                                 <tr class="info">
                                 		<td>${cmdprop.idCmdProp }</td>
                                        <td><b>${cmdprop.getClient().getGnr() }.&nbsp;${cmdprop.getClient().getPrenom() }&nbsp;${cmdprop.getClient().getNom() }</b></td>
                                        <td><b>${cmdprop.getCommentaire() }</b></td>
                                        <td>${cmdprop.getQte() }</td>
                                    	<td><fmt:formatDate value="${cmdprop.getDate()}" pattern="dd/MM/yyyy" />
                                 		</td>
                                 		<td>
                                 		<button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#modal1_${cmdprop.idCmdProp}"><i class="fa fa-trash-o "></i></button>
                                 		 <!-- Modal -->
			                            <div class="modal fade" id="modal1_${cmdprop.idCmdProp}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                                <div class="modal-dialog">
			                                    <div class="modal-content">
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                            <h4 class="modal-title" id="myModalLabel">CONFIRMATION!</h4>
			                                        </div>
			                                        <div class="modal-body">
			                                        <div class="panel panel-red">
									                        <div class="panel-heading">
									                            <div class="row">
									                                <div class="col-xs-3">
									                                    <i class="fa fa-warning fa-5x"></i>
									                                </div>
									                                <div class="col-xs-9 ">
									                               	<h3>Voulez-vous vraiment supprimer la Prop N�:&nbsp;<b>${cmdprop.idCmdProp}</b></h3>				                            

									                                </div>
									                            </div>
									                        </div>
									                    </div>
			                                         </div>
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			                                            <c:url value="/prop/deletcmdprop/${cmdprop.idCmdProp }" var="urlSuppr"/>
			                                            <a href="${urlSuppr }" class="btn btn-danger"><i class="fa fa-trash-o "></i> Spprimer!</a>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
                                 		</td>
                                    </tr>
                                </c:forEach>
                                    
                                </tbody>
                                <tfoot>
                                <th>
                                <c:url value="/prop/addcmdprop/${prop.idProp }" var="urlAddCmdprop"/>
								<a href="${urlAddCmdprop }" class="btn btn-info"><i class="fa  fa-plus"></i> Ajouter</a>
								</th>
                                </tfoot>
                            </table>
            

        </div>
    </div>
    <!-- /#wrapper -->
    

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   