<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body id="page-wrapper">


<div class="col-lg-12">
         <div class="row">
                <div class="page-headerpdf col-lg-12">
                    <h1 style="padding-top: 10px;"><c:out value="${prop.codeProp}"/></h1>
                </div>
            </div>

        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">

                                <li class="list-group-item">
                                    <label>Designation</label>${prop.designation}
                                </li>
                                <li class="list-group-item ">
                                    <label>Date d'Entr�e</label><fmt:formatDate value="${prop.getDate()}" pattern="dd/MM/yyyy" />
                                </li>
                                <li class="list-group-item">
                                    <label>Categorie</label>${prop.getCategory().getCode() }
                                </li>
                                <li class="list-group-item">
                                    <label>Entropot</label>${prop.getEntropot().getCode() }
                                </li>
                                <li class="list-group-item">
                                    <label>Quqntit�</label>${prop.qte }
                                </li>
                                <li class="list-group-item">
                                    <label>Reste</label>${prop.reste }
                                </li>
                                <li class="list-group-item">
                                    <label>Valeur</label>${prop.valeur }<b> DH</b>
                                </li>
                                
                            </ul>
                      </div>            
            </div>
             <div class="row">
                <div class="col-lg-12">
                    <h2 style="border-bottom: 1px solid #eee; color: #337ab7;">Re�u par :</h2>
                </div>
            </div>
            <table width="100%" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>N�</th>
                                        <th>Agent</th>
                                        <th>Commentaire</th>
                                        <th>Quantite</th>
                                    	<th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                               <c:forEach items="${cmdprops}" var="cmdprop">
                                 <tr class="info" style="page-break-inside: avoid;">
                                 		<td>${cmdprop.idCmdProp }</td>
                                        <td><b>${cmdprop.getClient().getGnr() }.&nbsp;${cmdprop.getClient().getPrenom() }&nbsp;${cmdprop.getClient().getNom() }</b></td>
                                        <td><b>${cmdprop.getCommentaire() }</b></td>
                                        <td>${cmdprop.getQte() }</td>
                                    	<td><fmt:formatDate value="${cmdprop.getDate()}" pattern="dd/MM/yyyy" />
                                 		</td>
                                    </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
            

        </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   