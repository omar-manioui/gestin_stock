<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Propri�t�s de la Fondation</h1>
                </div>
            </div>
            <div class="row alert alert-danger alert-dismissable center">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Impossible! La Quantit� retante en stock est inf�rieur � la quantit� demand�e !
          </div>
            
        <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        <c:if test="${prop.getIdProp()!=null }">
                        Modification Propri�t�
                        </c:if>
                        <c:if test="${prop.getIdProp()==null }">
                        Nauveau Propri�t�
                        </c:if>
                            
                        </div>
                        <div class="panel-body">
                        
                            <c:url value="/prop/enregistrer" var ="urlEnregistrer" />
							<f:form modelAttribute="prop" action="${urlEnregistrer }" enctype="multipart/form-data" role="form">
							<f:hidden path="idProp"/>
							<f:hidden path="reste"/>
							<f:hidden path="photo"/>
						    <div class="form-group form-row col-md-10">
						     <label>Nom de Propri�t�</label>
                             <f:input path="codeProp" class="form-control" placeholder="Nom de Propri�t�" />
                             
						    </div>
						    <div class="form-group form-row col-md-10">
						      <label>Designation</label>
                             <f:input path="designation" class="form-control" placeholder="Designation" />
						    </div>
						
						  <div class="form-group form-row col-md-10">
						    <label>Categorie</label>
                             <f:select class="form-control" path="category.idCategory" items="${categories }" itemLabel="code" itemValue="idCategory" />
						  </div>
						  <div class="form-group form-row col-md-10">
						    <label>Entropots</label>
                             <f:select class="form-control" path="entropot.idEntropot" items="${entropots }" itemLabel="code" itemValue="idEntropot" />
						  </div>
						  
						 
						  
						  <div class="form-group form-row col-md-10">
                               <label>Date d'Entr�e</label>
                                <div class="input-group date datepicker" data-date-format="mm/dd/yyyy">
				                    <f:input path="date" type='text' class="form-control" readonly="true"/>
				                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>
                           </div>
							<div class="form-group form-row col-md-10">
                                <label>Valeur de la Propri�t�</label>
                                <div class="input-group">
                                <f:input path="valeur" class="form-control numero" placeholder="Valeur de la Propri�t�" />
                            	<span class="input-group-addon"><b>DH</b></span>
                            	</div>
                            </div> 
						    <div class="form-group col-md-10">
						     <label>Stock</label>
                             <f:input path="qte" class="form-control numero" placeholder="Stock Actuel" />
						    </div>
                            
						  <div class="form-groupform-row col-md-10">
                             <label>Photo</label>
                            <input type="file" name="file" class="filestyle" data-btnClass="btn-primary">
                         </div>
                            </div>
                        <div class="panel-footer">
                        <div class="row">
                        <div  class="col-md-3"></div>
                        <div  class="col-md-6">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Enregister</button>
                            <a href="<c:url value="/prop/" />" class="btn btn-danger"> <i class="fa fa-arrow-left"></i> Annuler</a>
                        </div>
                        <div  class="col-md-3"></div>
                        </div>
                        </div>
                        </f:form>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>

    </div>
    <!-- /#wrapper -->
    
 
 
 <script type="text/javascript" src="<%=request.getContextPath() %>/resources/bootstrap-filestyle.min.js"></script>
 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   