<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Propri�t�s de la Fondation</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
	            <div class="col-lg-12">
	            <ol class="breadcrumb">
                	<li><a href="<c:url value="/prop/nouveau" />" ><i class="fa fa-plus  "></i> Ajouter</a></li>
                </ol>	            
	            </div>
            
            </div>
        <div class="row">
               

                        <div class="row">
                        <ul class="nav nav-tabs " style="margin: 10px;">
                                <li class="active"><a href="#profile" data-toggle="tab"><i class="fa fa-th-list  "></i></a>
                                </li>
                                <li ><a href="#home" data-toggle="tab"><i class="fa fa-th-large "></i></a>
                                </li>
                            </ul>
                          </div>
                            <div class="tab-content">
                                <div class="tab-pane fade" id="home">
                                <table width="100%" class="dataTables">
                                <thead hidden="true">
                                    <tr>
                                        <th>Article</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${props}" var="prop">
                                 <c:url value="/prop/details/${prop.getIdProp() }" var="urlArticler"/>
                                <tr class="col-md-3">
                                <td class="thumbnail" width="100%">
                                <div class="clickable-row"  data-href="${urlArticler }" style="cursor: pointer; ">
					                <img width="200px" height="200px" src="<%=request.getContextPath() %>/images/prop/${article.getPhoto() }" alt="">
					                <div class="caption">
					                  <h5 class="pull-right">${prop.getValeur() } DH</h5>
					                  <h4><a href="#">${prop.getCodeProp() }</a></h4>
					                  <small class="text-muted">Quantit� en Stock :<b> ${prop.getQte() }</b></small><br>
					                </div></div>
					                <div class="ratings">
					                  <c:url value="/prop/modifier/${prop.getIdProp() }" var="urlModifier"/>
								    <a href="${urlModifier }" class="btn btn-outline btn-primary "><i class="fa  fa-edit"></i></a>
					                  <button class="pull-right btn btn-outline btn-danger " data-toggle="modal" data-target="#modal_${prop.getIdProp()  }"><i class="fa fa-trash-o "></i></button>
								  <!-- Modal -->
			                            <div class="modal fade" id="modal_${prop.getIdProp()  }" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                                <div class="modal-dialog">
			                                    <div class="modal-content">
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                            <h4 class="modal-title" id="myModalLabel">CONFIRMATION!</h4>
			                                        </div>
			                                        <div class="modal-body">
			                                        <div class="panel panel-red">
									                        <div class="panel-heading">
									                            <div class="row">
									                                <div class="col-xs-3">
									                                    <i class="fa fa-warning fa-5x"></i>
									                                </div>
									                                <div class="col-xs-9 ">
									                               	<h3>Voulez-vous vraiment supprimer l'Article :&nbsp;<b>${prop.getCodeProp() }</b></h3>				                            

									                                </div>
									                            </div>
									                        </div>
									                    </div>
			                                         </div>
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			                                            <c:url value="/prop/supprimer/${prop.getIdProp()  }" var="urlSupprimer"/>
			                                            <a href="${urlSupprimer }" class="btn btn-danger"><i class="fa fa-trash-o "></i> Spprimer!</a>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
					                </div>
								  </td>
								</tr>
                                    
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                                </div>
                                <div class="tab-pane fade in active" id="profile">
                                <table width="100%" class="table table-striped table-hover dataTables">
                                <thead>
                                    <tr>
                                    	<th>N�</th>
                                        <th>Code</th>
                                        <th>Designation</th>
                                        <th class="text-center">Date d'Entr�e</th>
                                        <th class="text-center">Quantit�</th>
                                        <th class="text-rigth">Valeur</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${props}" var="prop">
                                <c:url value="/prop/details/${prop.getIdProp() }" var="urlArticler"/>
                                
                                 <tr>
                                 		<td class="clickable-row"  data-href="${urlArticler }" style="cursor: pointer; "><b>${prop.getIdProp() }</b></td>
                                        <td class="clickable-row"  data-href="${urlArticler }" style="cursor: pointer; "><b>${prop.getCodeProp() }</b></td>
                                        <td class="clickable-row"  data-href="${urlArticler }" style="cursor: pointer; ">${prop.getDesignation()}</td>
                                        <td class="text-center clickable-row"  data-href="${urlArticler }" style="cursor: pointer; "><fmt:formatDate value="${prop.getDate() }" pattern="dd/MM/yyyy" /></td>
                                        <td class="text-center clickable-row"  data-href="${urlArticler }" style="cursor: pointer; ">${prop.getQte() }</td>
                                        <td class="text-right clickable-row"  data-href="${urlArticler }" style="cursor: pointer; ">${prop.getValeur() }<b> DH</b></td>
                                 
                                        <td class="text-center ">
                                        <c:url value="/prop/modifier/${prop.getIdProp() }" var="urlModifier"/>
										<a href="${urlModifier }" class="btn btn-primary btn-circle"><i class="fa  fa-edit"></i></a>
										&nbsp;|&nbsp;
			                            <button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#modal1_${prop.getIdProp() }"><i class="fa fa-trash-o "></i></button>
			                            
			                            <!-- Modal -->
			                            <div class="modal fade" id="modal1_${prop.getIdProp() }" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                                <div class="modal-dialog">
			                                    <div class="modal-content">
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                            <h4 class="modal-title" id="myModalLabel">CONFIRMATION!</h4>
			                                        </div>
			                                        <div class="modal-body">
			                                        <div class="panel panel-red">
									                        <div class="panel-heading">
									                            <div class="row">
									                                <div class="col-xs-3">
									                                    <i class="fa fa-warning fa-5x"></i>
									                                </div>
									                                <div class="col-xs-9 ">
									                               	<h3>Voulez-vous vraiment supprimer l'Article :&nbsp;<b>${prop.getCodeProp() }</b></h3>				                            

									                                </div>
									                            </div>
									                        </div>
									                    </div>
			                                         </div>
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			                                            <c:url value="/prop/supprimer/${prop.getIdProp() }" var="urlSupprimer"/>
			                                            <a href="${urlSupprimer }" class="btn btn-danger"><i class="fa fa-trash-o "></i> Spprimer!</a>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
										</td>
                                    </tr>
                                    
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                                </div>
                            </div>
                                  

            </div>

        </div>
    </div>
    <script >
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
    </script>
    

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   