<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Consommations</h1>
                </div>
            </div>
            <div class="row">
	            <div class="col-lg-12">
	            <ol class="breadcrumb">
                	<li><a href="<c:url value="/consommation/nouveau" />" ><i class="fa fa-plus  "></i> Ajouter</a></li>
                </ol>	            
	            </div>
            
            </div>
        <div class="row">
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-hover dataTables">
                                <thead>
                                    <tr>
                                    	<th>N�</th>
                                        <th>R�f Facture</th>
                                        <th>Designation</th>
                                        <th>Fournisseur</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${consos}" var="conso">
                                <c:url value="/consommation/details/${conso.getIdConsommation() }" var="urlConsommation"/>
                                 <tr class="odd gradeX">
                                 		<td class="clickable-row" data-href="${urlConsommation }" style="cursor: pointer;">${conso.getIdConsommation() }</td>
                                        <td class="clickable-row" data-href="${urlConsommation }" style="cursor: pointer;">${conso.getCode() }</td>
                                      	<td class="clickable-row" data-href="${urlConsommation }" style="cursor: pointer;">${conso.designation }</td>
                                        <td class="clickable-row" data-href="${urlConsommation }" style="cursor: pointer;">${conso.getFournisseur() }</td>
                                        <td class="clickable-row" data-href="${urlConsommation }" style="cursor: pointer;"><fmt:formatDate value="${conso.getDate() }" pattern="MMM yyyy" /></td>
                                        <td class="text-center">
                                        <c:url value="/consommation/modifier/${conso.getIdConsommation()}" var="urlModifier"/>
										<a href="${urlModifier }" class="btn btn-primary btn-circle"><i class="fa  fa-edit"></i></a>
										&nbsp;|&nbsp;
			                            <button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#modal_${conso.getIdConsommation() }"><i class="fa fa-trash-o "></i></button>
			                            
			                            <!-- Modal -->
			                            <div class="modal fade" id="modal_${conso.getIdConsommation() }" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                                <div class="modal-dialog">
			                                    <div class="modal-content">
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                            <h4 class="modal-title" id="myModalLabel">CONFIRMATION!</h4>
			                                        </div>
			                                        <div class="modal-body">
			                                       <div class="panel panel-red">
									                        <div class="panel-heading">
									                            <div class="row">
									                                <div class="col-xs-3">
									                                    <i class="fa fa-warning fa-5x"></i>
									                                </div>
									                                <div class="col-xs-9 ">
									                               	<h3>Voulez-vous vraiment supprimer la Consommation :&nbsp;<b>${conso.getCode() }</b></h3>				                            

									                                </div>
									                            </div>
									                        </div>
									                    </div>
			                                         </div>
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			                                            <c:url value="/consommation/supprimer/${conso.getIdConsommation() }" var="urlSupprimer"/>
			                                            <a href="${urlSupprimer }" class="btn btn-danger"><i class="fa fa-trash-o "></i> Spprimer!</a>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
										</td>
                                    </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                        </div>
            </div>
        </div>
    </div>
 <script >
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
    </script>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   