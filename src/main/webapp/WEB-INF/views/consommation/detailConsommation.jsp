<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
        <div class="row">
                <div class="page-header col-lg-12">
                <div class="col-lg-6">
                    <h1><c:out value="${conso.getCode()}"/></h1>
                </div>
                <div class="col-lg-6 text-right">
                    <c:url value="/consommation/modifier/${conso.getIdConsommation() }" var="urlModifier"/>
					<a href="${urlModifier }" class="btn btn-primary"><i class="fa  fa-edit"></i> Modifier</a>
					</div>
                </div>
            </div>

        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">

                                <li class="list-group-item">
                                    <label>Designation</label>${conso.getDesignation()}
                                </li>
                                <li class="list-group-item">
                                    <label>Genre</label>${conso.getGenre() }
                                </li>
                                <li class="list-group-item">
                                    <label>Fournisseur</label>${conso.getFournisseur() }
                                </li>
                            </ul>
                      </div>     
                      <div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">
                                <li class="list-group-item ">
                                    <label>Date</label><fmt:formatDate value="${conso.getDate() }" pattern="MMM yyyy" /> 
                                </li>
                                <li class="list-group-item">
                                    <label>Montant</label>${conso.getMontant() }<b> DH</b>
                                </li>
                                
                            </ul>
                      </div>            
            </div>
            
            <ol class="breadcrumb text-right">
                	<li><a target="_blank" href="<c:url value="/consommation/detailspdf/${conso.getIdConsommation() }" />" ><i class="fa  fa-print   "></i> Imprimer</a></li>
                </ol> 
           
            

        </div>
    </div>
    

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   