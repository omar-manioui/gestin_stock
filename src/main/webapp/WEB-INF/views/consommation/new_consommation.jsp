<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Consommation</h1>
                </div>
            </div>
            
        <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        <c:if test="${conso.idConsommation !=null }">
                        Modification Consommation
                        </c:if>
                        <c:if test="${conso.idConsommation ==null }">
                        Nauvelle Consommation
                        </c:if>
                            
                        </div>
                        <c:url value="/consommation/enregistrer" var ="urlSavec" />
							<f:form modelAttribute="conso" method="POST" action="${urlSavec }" role="form">
                        <div class="panel-body">
                        
                            
							<f:hidden path="idConsommation"/>
						    <div class="form-group form-row col-md-10">
						     <label>R�f Facture</label>
                             <f:input path="code" class="form-control" placeholder="R�f Facture"/>
						    </div>
						    <div class="form-group form-row col-md-10">
							<label>Designation</label>
							<f:input path="designation" class="form-control" placeholder="Designation"/>							 	
							</div>
							<div class="form-row">
							<div class="form-group col-md-5">
						     <label>Genre</label>
                             <f:select path="genre" class="form-control" onchange="consommationHandler(this)">
                             	<option value="Electricite">Electricit�</option>
                                <option value="Eau">Eau</option>
                                <option value="Internet">Internet</option>
                                <option value="Telephone">Telephone</option>
                                <option value="Carburant">Carburant</option>
                             </f:select>
						    </div>
							 <div class="form-group col-md-5">
						     <label>Fournisseur</label>
                             <f:select id="fournisseur" path="fournisseur" class="form-control">
                             	<option value="RADEEF">RADEEF</option>
                                <option value="IAM">IAM</option>
                                <option value="INWI">INWI</option>
                                <option value="Orange">Orange</option>
                                <option value="Carburant">Carburant</option>
                             </f:select>
						    </div>
						    </div>
							<div class="form-group form-row col-md-10">
                                <label>Montant</label>
                                <div class="input-group">
                                <f:input path="montant" class="form-control numero" placeholder="Montant" />
                                <span class="input-group-addon"><b>DH</b></span>
                                </div>
                            	</div>
                            	<div class="form-group form-row col-md-10">
                               <label>Date</label>
                                <div class="input-group date datepicker1" data-date-format="mm/dd/yyyy">
				                    <f:input path="date" type='text' class="form-control" readonly="true"/>
				                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>
                           </div>
                            </div>
                            
                            
                        <div class="panel-footer">
                        <div class="row">
                        <div  class="col-md-3"></div>
                        <div  class="col-md-6">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Enregister</button>
                            <a href="<c:url value="/consommation/" />" class="btn btn-danger"> <i class="fa fa-arrow-left"></i> Annuler</a>
                        </div>
                        <div  class="col-md-3"></div>
                        </div>
                        </div>
                        </f:form>
                    </div>
                </div>
            </div>

        </div>

    </div>
    
<script>
    function consommationHandler(select){
    	if(select.value == 'Internet' || select.value == 'Telephone'){
    		internet();
    	}else if(select.value == 'Electricite' || select.value == 'Eau'){
    		radeef();
    	}else if(select.value == 'Carburant'){
    		carburant();
    	}}
    function internet(){
    	var fournisseur = document.getElementById('fournisseur');
    	fournisseur.value = 'IAM';
    	}
    function radeef(){
    	var fournisseur = document.getElementById('fournisseur');
    	fournisseur.value = 'RADEEF';
    	}
    function carburant(){
    	var fournisseur = document.getElementById('fournisseur');
    	fournisseur.value = 'Carburant';
    	}
    </script>
    
 
 
 <script type="text/javascript" src="<%=request.getContextPath() %>/resources/bootstrap-filestyle.min.js"></script>
 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   