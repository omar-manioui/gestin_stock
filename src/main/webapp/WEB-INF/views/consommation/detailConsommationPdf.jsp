<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

        <!-- Page Content -->
        <div id="page-wrapper">
        <div class="row">
                <div class="page-headerpdf col-lg-12">
                <div class="col-lg-6">
                    <h1><c:out value="${conso.getCode()}"/></h1>
                </div>
            </div>

        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">

                                <li class="list-group-item">
                                    <label>Designation</label>${conso.getDesignation()}
                                </li>
                                <li class="list-group-item">
                                    <label>Genre</label>${conso.getGenre() }
                                </li>
                                <li class="list-group-item">
                                    <label>Fournisseur</label>${conso.getFournisseur() }
                                </li>
                                <li class="list-group-item ">
                                    <label>Date</label><fmt:formatDate value="${conso.getDate() }" pattern="MMM yyyy" /> 
                                </li>
                                <li class="list-group-item">
                                    <label>Montant</label>${conso.getMontant() }<b> DH</b>
                                </li>
                            </ul>
                      </div>              
            </div>
           
            

        </div>
    </div>
    

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   