<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tableau De Bord</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
               
               <div class="row">
                <div class="col-lg-12">
                <c:if test="${stckAlertes.size()>0 }">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-warning fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                               
                                    <div class="huge">${stckAlertes.size() }</div>
                                    <div>Alertes de Stock!</div>
                                </div>
                            </div>
                        </div>
                        <a href="${alertesURL }">
                            <div class="panel-footer">
                                <span class="pull-left">Voir les détails</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                    </c:if>
                    </div>
                    </div>
                    
                <div class="row">
                <div class="col-lg-7">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Planning
                        </div>
                        <div class="panel-body">
                        <div id="FullCalendar"></div> 
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <ul class="nav nav-tabs ">
                                <li class="active"><a href="#profile" data-toggle="tab">Lundi</a>
                                </li>
                                <li ><a href="#home" data-toggle="tab">Mardi</a>
                                </li>
                                <li ><a href="#home" data-toggle="tab">Mcredi</a>
                                </li>
                                <li ><a href="#home" data-toggle="tab">Jeudi</a>
                                </li>
                                <li ><a href="#home" data-toggle="tab">Vdredi</a>
                                </li>
                            </ul>
                        </div>
                        <div class="panel-body">
                        
                           <ul class="timeline">
                                <li>
                                    <div class="timeline-badge"><h5 >08:00<br>08:10</h5>
                                    </div>
                                    <div class="timeline-panel">
                                            <h4 class="timeline-title">إستقبال</h4>
                                            <p><small class="text-muted"><i class="fa fa-clock-o"></i> 10 min</small>
                                            </p>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-badge warning"><h5 >08:10<br>08:20</h5>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">السلم</h4>
                                            <p><small class="text-muted"><i class="fa fa-clock-o"></i> 10 min</small>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                 <li >
                                    <div class="timeline-badge info"><h5 >08:30<br>08:40</h5>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">مكاتب</h4>
                                            <p><small class="text-muted"><i class="fa fa-clock-o"></i> 10 min</small>
                                            </p>
                                        </div>
                                    </div>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge danger"><h5 >08:40<br>09:00</h5>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">الصالون</h4>
                                            <p><small class="text-muted"><i class="fa fa-clock-o"></i> 20 min</small>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                 <li >
                                    <div class="timeline-badge info"><h5 >09:00<br>10:00</h5>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">قاعة الإجتماع</h4>
                                            <p><small class="text-muted"><i class="fa fa-clock-o"></i> 1 h</small>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-badge success"><h5 >10:00<br>10:15</h5>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">تراس</h4>
                                            <p><small class="text-muted"><i class="fa fa-clock-o"></i> 15 min</small>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
                
                
                </div>
                
                <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="${clientURL }"><i class="fa fa-users fa-fw"></i> Agents</a>
                        </div>
                        <div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-hover dataTables1">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th class="text-center">Tel</th>
                                        <th class="text-center">E-mail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${clients}" var="client">
                                <c:url value="/client/details/${client.getIdClient() }" var="urlclient"/>
                                 <tr class="odd gradeX clickable-row" data-href="${urlclient }" style="cursor: pointer;">
                                        <td><b>${client.getGnr() }.</b>&nbsp;${client.getPrenom() }&nbsp;${client.getNom() }</td>
                                        <td class="text-center">${client.getTel() }</td>
                                        <td class="text-center">${client.getMail() }</td>
                                        
                                </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="${fournisseurURL }"><i class="fa fa-users fa-fw"></i> Fournisseurs</a>
                        </div>
                        <div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-hover dataTables1">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th class="text-center">Tel</th>
                                        <th class="text-center">E-mail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${fournisseurs}" var="fournisseur">
                                <c:url value="/fournisseur/details/${fournisseur.getIdFournisseur() }" var="urlfournisseur"/>
                                 <tr class="odd gradeX clickable-row" data-href="${urlfournisseur }" style="cursor: pointer;" data-href="${urlclient }" style="cursor: pointer;">
                                        <td>${fournisseur.getGnr() }.&nbsp;${fournisseur.getPrenom() }&nbsp;${fournisseur.getNom() }</td>
                                        <td class="text-center">${fournisseur.getTel() }</td>
                                        <td class="text-center">${fournisseur.getMail() }</td>
                                        
                                </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div></div>
                <div class="row">
                <div class="col-lg-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="${keysUrl }"><i class="fa fa-users fa-fw"></i> Clés</a>
                        </div>
                        <div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-hover dataTables1">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th class="text-center">Nom</th>
                                        <th class="text-center">Reste</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${keys}" var="key">
                                <c:url value="/prop/keydetails/${key.idCle }" var="urlKey"/>
                                 <tr class="odd gradeX clickable-row" data-href="${urlKey }" style="cursor: pointer;">
                                        <td>${key.idCle }</td>
                                        <td class="text-center">${key.code}</td>
                                        <td class="text-center">${key.reste }</td>
                                        
                                </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </div>



        </div>
        <!-- /#page-wrapper -->

    </div>
    <script src='<%=request.getContextPath() %>/resources/fullcalendar/dist/fullcalendar.js'></script>
    <script src='<%=request.getContextPath() %>/resources/fullcalendar/dist/locale/fr.js'></script>
    <c:url value="/" var="url"/>
    <script>
    $(document).ready(function() {
        $('#FullCalendar').fullCalendar({
			locale: 'fr',
        	header: {
				left: 'title',
				center: 'month,listMonth',
				right: 'prev,next '
			},
            theme: 'bootstrap3',
            editable: false,
            events: '${url }/calendar'
        }); });
    
    </script>
     <script >
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
    </script>
    <script>
    var dEvent = $('.event');

 // var h = dEvent.attr('data-duration');
 // var pos = dEvent.attr('data-start');



 $('.event').each(function() {
   var $this = $(this);
   var h = $this.attr('data-duration');
   var pos = $this.attr('data-start');
   $this.css({
     'height' : h * 60,
     'top' : pos * 60
   });
 });

 $(window).resize(function() {
   if ($(window).width() < '650') {
     $('.event-column').css('margin-left', "60px");
   } else {
     $('.event-column').css('margin-left', "0");
     $('.event-column').css('left', '0')
   }
 });


   if ($(window).width() < '650') {
     $('.event-column').css('margin-left', "60px");
   }

 $("#goRight").click(function(event) {
   event.preventDefault();
   var left = $('.event-column').css('left');
   var leftVal = parseInt(left.replace("px", ""));
   if( leftVal === 0 ) {
     $('.event-column').css('left', "-114px");
   } else if (leftVal > '-570') {
     var newPos = (leftVal - 114);
     $('.event-column').css({
       left : newPos + 'px'
     });
    }
   $("#goLeft").css("opacity", "1");
 });

 $("#goLeft").click(function(event) {
   event.preventDefault();
   
   var left = $('.event-column').css('left');
   var leftVal = parseInt(left.replace("px", ""));
   if( leftVal === 0 ) {
     event.preventDefault();
   } else if (leftVal >= '-684' ) {
     var newPos = (leftVal + 114);
     $('.event-column').css({
       left : newPos + 'px'
     });
    }
   $("#goLeft").css("opacity", "1");
 });



    </script>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   