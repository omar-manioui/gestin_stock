<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Articles</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        <c:if test="${article.getIdArticle()!=null }">
                        Modification Article
                        </c:if>
                        <c:if test="${article.getIdArticle()==null }">
                        Nauveau Article
                        </c:if>
                            
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        
                            <c:url value="/article/enregistrer" var ="urlEnregistrer" />
							<f:form modelAttribute="article" action="${urlEnregistrer }" enctype="multipart/form-data" role="form">
							<f:hidden path="idArticle"/>
							<f:hidden path="photo"/>
						    <div class="form-group form-row col-md-10">
						     <label>Nom d'Article</label>
                             <f:input path="codeArticle" class="form-control" placeholder="Nom d'Article" />
                             
						    </div>
						    <div class="form-group form-row col-md-10">
						      <label>Designation</label>
                             <f:input path="designation" class="form-control" placeholder="Designation" />
						    </div>
						  <div class="form-group form-row col-md-10">
						    <label>Categorie</label>
                             <f:select class="form-control" path="category.idCategory" items="${categories }" itemLabel="code" itemValue="idCategory" />
						  </div>
						  <div class="form-group form-row col-md-10">
						    <label>Entropots</label>
                             <f:select class="form-control" path="entropot.idEntropot" items="${entropots }" itemLabel="code" itemValue="idEntropot" />
						  </div>
						  
						  <div class="form-group form-row col-md-10">
                               <label>Prix Unitaire HT</label>
                               <div class="input-group">
                               <f:input id="prixUnitaireHT" path="prixUnitaireHT" class="form-control" placeholder="Prix unitaire HT"/>
                           		<span class="input-group-addon"><b>DH</b></span>
                           		</div>
                           </div>
							<div class="form-group form-row col-md-10">
                                <label>TVA</label>
                                <div class="input-group">
                                <f:input  id="tauxTva" path="tauxTva" class="form-control numero" placeholder="TVA"/>
                                <span class="input-group-addon"><b>%</b></span>
                                </div>
                            </div>
							<div class="form-group form-row col-md-10">
                                <label>Prix unitaire TTC </label>
                                <div class="input-group">
                                <f:input id="prixUnitaireTTC" path="prixUnitaireTTC"  disabled="" class="form-control numero" placeholder="Prix unitaire TTC" />
                            	<span class="input-group-addon"><b>DH</b></span>
                            	</div>
                            </div> 
						    <div class="form-row">
						    <div class="form-group col-md-5">
						     <label>Stock</label>
                             <f:input id="stock" path="stock" class="form-control numero" placeholder="Stock Actuel" />
						    </div>
						    <div class="form-group col-md-5">
						      <label>Stock Minimal</label>
                             <f:input id="stockMin" path="stockMin" class="form-control numero" placeholder="Stock Minimal" />
						    </div>
						  </div>
                            
						  <div class="form-groupform-row col-md-10">
                             <label>Photo</label>
                            <input type="file" name="file" class="filestyle" data-btnClass="btn-primary">
                         </div>
                            </div>
                        <div class="panel-footer">
                        <div class="row">
                        <div  class="col-md-3"></div>
                        <div  class="col-md-6">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Enregister</button>
                            <a href="<c:url value="/article/" />" class="btn btn-danger"> <i class="fa fa-arrow-left"></i> Annuler</a>
                        </div>
                        <div  class="col-md-3"></div>
                        </div>
                        </div>
                        </f:form>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>

    </div>
    <!-- /#wrapper -->
    
 
 
 <script type="text/javascript" src="<%=request.getContextPath() %>/resources/bootstrap-filestyle.min.js"></script>
 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   