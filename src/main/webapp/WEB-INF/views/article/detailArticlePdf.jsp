<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body id="page-wrapper">


<div class="col-lg-12">
         <div class="row">
                <div class="page-headerpdf col-lg-12">
                    <h1 style="padding-top: 10px;"><c:out value="${article.getCodeArticle()}"/></h1>
                </div>
            </div>

        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">

                                <li class="list-group-item">
                                    <label>Designation</label>${article.getDesignation()}
                                </li>
                                <li class="list-group-item">
                                    <label>Categorie</label>${article.getCategory().getCode() }
                                </li>
                                <li class="list-group-item">
                                    <label>Entropot</label>${article.getEntropot().getCode() }
                                </li>
                                <li class="list-group-item ">
                                    <label>Prix HT</label>${article.getPrixUnitaireHT() }<b> DH</b>
                                </li>
                                <li class="list-group-item">
                                    <label>TVA</label>${article.getTauxTva() }<b> %</b>
                                </li>
                                <li class="list-group-item">
                                    <label>Prix TTC</label>${article.getPrixUnitaireTTC() }<b> DH</b>
                                </li>
                                <li class="list-group-item">
                                    <label>Stock Minimal</label>${article.getStockMin() }
                                </li>
                                <li class="list-group-item">
                                    <label>Stock Actuel</label>${article.getStock() }
                                </li>
                                
                            </ul>
                      </div>            
            </div>
             <div class="row">
                <div class="col-lg-12">
                    <h2 style="border-bottom: 1px solid #eee; color: #337ab7;">Mouvment de Stock</h2>
                </div>
            </div>
            <table width="100%" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Bénéficiaire</th>
                                        <th>Commentaire</th>
                                        <th>Quantite</th>
                                    	<th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${mvtstksarticle}" var="mvtstkarticle">
                                 <tr class="info" style="page-break-inside: avoid;">
                                        <td><b>Admin</b></td>
                                        <td><b>${mvtstkarticle.getCommentaire() }</b></td>
                                        <td>${mvtstkarticle.getQuantite() }</td>
                                 		<td><fmt:formatDate value="${mvtstkarticle.getDateMvt()}" pattern="dd/MM/yyyy" />
                                 		</td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${lcmdarticles}" var="lcmdarticle">
                                 <tr class="danger" style="page-break-inside: avoid;">
                                        <td><b>${lcmdarticle.getCommandeClient().getClient().getGnr() }.&nbsp;${lcmdarticle.getCommandeClient().getClient().getPrenom() }&nbsp;${lcmdarticle.getCommandeClient().getClient().getNom() }</b></td>
                                        <td><b>${lcmdarticle.getCommentaire() }</b></td>
                                        <td>-${lcmdarticle.getQte() }</td>
                                    	<td><fmt:formatDate value="${lcmdarticle.getCommandeClient().getDateCommande()}" pattern="dd/MM/yyyy" />
                                 		</td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${lcmdarticlesf}" var="lcmdarticlef">
                                 <tr class="success" style="page-break-inside: avoid;">
                                        <td><b>${lcmdarticlef.getCommandeFournisseur().getFournisseur().getGnr() }.&nbsp;${lcmdarticlef.getCommandeFournisseur().getFournisseur().getPrenom() }&nbsp;${lcmdarticlef.getCommandeFournisseur().getFournisseur().getNom() }</b></td>
                                 		<td><b>${lcmdarticlef.getCommentaire() }</b></td>
                                        <td>${lcmdarticlef.getQte() }</td>
                                 		<td><fmt:formatDate value="${lcmdarticlef.getCommandeFournisseur().getDateCommande()}" pattern="dd/MM/yyyy" />
                                 		</td>
                                    </tr>
                                </c:forEach>
                                    
                                </tbody>
                            </table>
            

        </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   