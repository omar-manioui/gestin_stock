<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 
        <div id="page-wrapper">
        <div class="row">
        		<div class="col-lg-2"><img src="<%=request.getContextPath() %>/images/client/${user.getPhoto() }" width="100px" height="130px" style="margin: 10px;"/>
        		</div>
                <div class="page-header col-lg-10">
                <div class="col-lg-6">
                    <h1 ><c:out value="${user.getPrenom()} ${user.getNom() }"/></h1>
                </div>
                <div class="col-lg-6 text-right">
                    <c:url value="/edituser" var="urlModifier"/>
					<a href="${urlModifier }" class="btn btn-primary"><i class="fa  fa-edit"></i> Modifier</a>
					</div>
                 </div>
            </div>
        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">
                                <li class="list-group-item"> 
                                <i class="icon fa fa-envelope-o"></i>
                                    <label>E-mail</label>${user.getMail() }
                                </li>
                            </ul>
                      </div>     
                      <div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">
                                <li class="list-group-item">
                                 <i class="icon fa  fa-unlock-alt"></i>
                                   <label>Mot de Passe</label><input type="password" value="${user.getMotDePasse() }" readonly/>
                                </li>
                                
                            </ul>
                      </div>    
            </div> 

        </div>
    </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   