<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <div id="page-wrapper">
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Administrateur</h1>
                </div>
            </div>
            
        <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        Administrateur
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        
                            <c:url value="/enregistrer" var ="urlEnregistrer" />
							<f:form modelAttribute="user" action="${urlEnregistrer }" enctype="multipart/form-data" role = "form">
							<f:hidden path="idUtilisateur"/>
							<f:hidden path="photo"/>
							<f:hidden path="actived"/>
						  <div class="form-row">
						    <div class="form-group col-md-5">
						     <label>Pr�nom</label>
                             <f:input path="prenom" class="form-control" placeholder="Pr�nom" />
						    </div>
						    <div class="form-group col-md-6">
						      <label>Nom</label>
                             <f:input path="nom" class="form-control" placeholder="Nom" />
						    </div>
						  </div>
						  <div class="form-group form-row col-md-11">
						    <label>E-mail</label>
                             <f:input path="mail" class="form-control" placeholder="E-mail" />
						  </div>
						 <div class="form-group form-row col-md-11">
						    <label>Mot de Passe</label>
                             <f:input path="motDePasse" class="form-control" placeholder="Mot de Passe" />
						  </div>
						  <div class="form-groupform-row col-md-11">
                             <label>Photo</label>
                            <input type="file" name="file" class="filestyle" data-btnClass="btn-primary">
                         </div>	
                            
                        </div>
                        <div class="panel-footer">
                        <div class="row">
                        <div  class="col-md-3"></div>
                        <div  class="col-md-6">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Enregister</button>
                            <a href="<c:url value="/home" />" class="btn btn-danger"> <i class="fa fa-arrow-left"></i> Annuler</a>
                        </div>
                        <div  class="col-md-3"></div>
                        </div>
                        </div>
                        </f:form>
                    </div>
                </div>
            </div>
        </div>
    </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   