<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 
        <div id="page-wrapper">
        <div class="row">
        		<div class="col-lg-2"><img src="<%=request.getContextPath() %>/images/client/${client.getPhoto() }" width="100px" height="130px" style="margin: 10px;"/>
        		</div>
                <div class="page-header col-lg-10">
                <div class="col-lg-6">
                    <h1 ><c:out value="${client.getGnr() }. ${client.getPrenom()} ${client.getNom() }"/></h1>
                </div>
                <div class="col-lg-6 text-right">
                    <c:url value="/client/modifier/${client.getIdClient() }" var="urlModifier"/>
					<a href="${urlModifier }" class="btn btn-primary"><i class="fa  fa-edit"></i> Modifier</a>
					</div>
                 </div>
            </div>
        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">

                                <li class="list-group-item">
                                <i class="icon fa fa-phone"></i>
                                    <label>Telephone </label>${client.getTel()}
                                </li>
                                <li class="list-group-item"> 
                                <i class="icon fa fa-envelope-o"></i>
                                    <label>E-mail</label>${client.getMail() }
                                </li>
                            </ul>
                      </div>     
                      <div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">
                                <li class="list-group-item ">
                                <i class="icon fa fa-map-marker"></i>
                                    <label>Adresse</label>${client.getAdresse() }&nbsp;${client.getAdresse2() } 
                                </li>
                                <li class="list-group-item">
                                 <i class="icon fa fa-map-marker"></i>
                                    <label>Ville</label>${client.getVille() }
                                </li>
                                
                            </ul>
                      </div>    
            </div>  
            <ol class="breadcrumb text-right">
                	<li><a target="_blank" href="<c:url value="/client/detailspdf/${client.getIdClient() }" />" ><i class="fa  fa-print   "></i> Imprimer</a></li>
                </ol> 
             
             <div class="row">
                <div class="col-lg-12">
                    <h2 style="border-bottom: 1px solid #eee; color: #337ab7;">Articles Consommés</h2>
                </div>
            </div>
            <div class="row">
            	 <c:url value="/client/details_par_date/${client.idClient }" var ="urlEnregistrer" />
				<f:form modelAttribute="periode" action="${urlEnregistrer }" role = "form">
					<div class="form-group form-row col-md-4">
                                <div class="input-group date datepicker" data-date-format="mm/dd/yyyy">
                                	<span class="input-group-addon">De</span>
				                    <f:input path="de" type='text' class="form-control" readonly="true"/>
				                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>
                  	</div><div class="form-group form-row col-md-1"></div>
                  	<div class="form-group form-row col-md-4">
                                <div class="input-group date datepicker" data-date-format="mm/dd/yyyy">
                                <span class="input-group-addon">A</span>
				                    <f:input path="a" type='text' class="form-control" readonly="true"/>
				                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>
                  	</div>
                  	<div class="col-md-3">
                  	<button type="submit" class="btn btn-info"><i class="fa fa-search "></i> Recherche</button>
                  	<a href="<c:url value="/client/details/${client.idClient }" />" class="btn btn-warning"> <i class="fa fa-times"></i> Annuler</a>
                  	</div>
                  	
				</f:form>
             </div>
            <table width="100%" class="table table-striped table-hover dataTables">
                                <thead>
                                    <tr>
                                        <th>Article</th>
                                        <th>Commentaire</th>
                                        <th>Quantite</th>
                                    	<th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${lcmdarticles}" var="lcmdarticle">
                                 <tr class="info">
                                        <td><b>${lcmdarticle.getArticle().getCodeArticle() }</b></td>
                                        <td><b>${lcmdarticle.getCommentaire() }</b></td>
                                        <td>${lcmdarticle.getQte() }</td>
                                 		<td><fmt:formatDate value="${lcmdarticle.getCommandeClient().getDateCommande()}" pattern="dd/MM/yyyy" />
                                 		</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
            

        </div>
    </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   