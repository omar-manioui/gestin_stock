


    <!-- Metis Menu Plugin JavaScript -->
    <script src="<%=request.getContextPath() %>/resources/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<%=request.getContextPath() %>/resources/dist/js/sb-admin-2.js"></script>
	<!-- DataTables JavaScript -->
    <script src="<%=request.getContextPath() %>/resources/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->

	<script type="text/javascript" src="<%=request.getContextPath() %>/resources/bootstrap-filestyle.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/resources/vendor/bootstrap/js/bootstrap-datepicker.js"></script>
    <script>
    
    $(document).ready(function() {
    	jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    	    "date-uk-pre": function ( a ) {
    	        if (a == null || a == "") {
    	            return 0;
    	        }
    	        var ukDatea = a.split('/');
    	        return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    	    },
    	 
    	    "date-uk-asc": function ( a, b ) {
    	        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    	    },
    	 
    	    "date-uk-desc": function ( a, b ) {
    	        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    	    }
    	} );
        $('.dataTables').dataTable( {
            language: {
            	search: "_INPUT_",
                searchPlaceholder: "Recherch",
                "info":           "Affiche _START_ � _END_ de _TOTAL_ El�ments",
                "emptyTable":     "Aucun El�ment disponible",
                "infoEmpty":      "Affiche 0 � 0 de 0 El�ments",
                "infoFiltered":   "(Filtr� � partir des _MAX_ total El�ments)",
                "zeroRecords":    "Aucun El�ment correspondant trouv�",
                "lengthMenu": "_MENU_",
		        "paginate": {
		        	 first:    'Premier',
		                previous: 'Precedent',
		                next:     'Suivant',
		                last:     'Dernier'
		        }
		            },
        columnDefs: [
            { type: 'date-uk', targets: 0 }
          ]
        } );
    });
    </script>
    <script >
    $(document).ready(function() {
    $('.dataTables1').dataTable( {
        language: {
        	search: "_INPUT_",
            searchPlaceholder: "Recherch",
            "info":           "",
            "emptyTable":     "Aucun El�ment disponible",
            "infoEmpty":      "",
            "infoFiltered":   "",
            "zeroRecords":    "Aucun El�ment correspondant trouv�",
            "lengthMenu": "_MENU_",
	        "paginate": {
	        	 first:    '&lt;&lt;',
	                previous: '&lt;',
	                next:     '&gt;',
	                last:     '&gt;&gt;'
	        }
	            },
    columnDefs: [
        { type: 'date-uk', targets: 0 }
      ]
    } );
});
</script>
    <script>
    $(function () {
    	  $(".datepicker").datepicker({
    	        autoclose: true, 
    	        todayHighlight: true
    	  });
    	  $(".datepicker").datepicker("setDate", new Date());
    	});
</script>
<script>
    $(function () {
    	  $(".datepicker1").datepicker({
    		    viewMode: "months", 
    		    minViewMode: "months",
    	        autoclose: true, 
    	        todayHighlight: true
    	  });
    	  $(".datepicker1").datepicker("setDate", new Date());
    	});
</script>
    
</body>

</html>