<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="ar">
<head>
	<meta charset="utf-8">
    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath() %>/resources/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

</head>
<body>
<div class="row" style="border-bottom: 1px solid #eee;">
<label style="width: 20px;"></label>
<label class="text-center" style="width: 320px;"><h5>Royaume du Maroc<br>Fondation Mohamed VI des Oulema Africans</h5></label>
<label class="text-center" style="width: 70px;"><img alt="" src="<%=request.getContextPath() %>/resources/launcher.png" height="70px" width="70px"/></label>
<label class="text-center"style="width: 300px;"><h5>المملكة المغربية<br> مؤسسة محمد السادس للعلماء الأفارقة</h5></label>
</div>
</body>
</html>