<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">

       
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 

        <!-- Page Content -->
        <div id="page-wrapper">
        <div class="row">
                <div class="page-header col-lg-12">
                <div class="col-lg-6">
                    <h1 ><c:out value="${cmd.getCode() }"/></h1>
                </div>
                    <div class="col-lg-6 text-right">
                    <c:url value="/commande_fournisseur/modifier/${cmd.getIdCommandeFournisseur() }" var="urlModifier"/>
					<a href="#" class="btn btn-primary"><i class="fa  fa-edit"></i> Modifier</a>
					</div>
                </div>
            </div>

        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">

                                <li class="list-group-item" style="border-bottom: 1px solid #eee;">
                                <i class="icon fa fa-user"></i>
                                    <label>Fournisseur </label>${cmd.getFournisseur().getGnr() }. ${cmd.getFournisseur().getPrenom()} ${cmd.getFournisseur().getNom() }
                                </li>
                            </ul>
                      </div>     
                      <div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">
                                <li style="border-bottom: 1px solid #eee;" >
                                <i class="icon fa fa-calendar"></i>
                                    <label>Date</label><fmt:formatDate value="${cmd.getDateCommande() }" pattern="dd/MM/yyyy" /> 
                                </li>
                                
                            </ul>
                      </div>            
            </div>
            <ol class="breadcrumb text-right">
                	<li><a target="_blank" href="<c:url value="/commande_fournisseur/detailspdf/${cmd.getIdCommandeFournisseur() }" />" ><i class="fa  fa-print   "></i> Imprimer</a></li>
                </ol> 
             <div class="row">
                <div class="col-lg-12">
                    <h2 style="border-bottom: 1px solid #eee; color: #337ab7;">Articles</h2>
                </div>
            </div>
            <table width="100%" class="table table-striped table-hover dataTables">
                                <thead>
                                    <tr>
                                        <th>Article</th>
                                        <th>Commentaire</th>
                                        <th>Quantite</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${lcmd}" var="lcmdarticle">
                                 <tr class="info">
                                        <td><b>${lcmdarticle.getArticle().getCodeArticle() }</b></td>
                                        <td><b>${lcmdarticle.getCommentaire() }</b></td>
                                        <td>${lcmdarticle.getQte() }</td>
                                 		
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
            

        </div>
    </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   