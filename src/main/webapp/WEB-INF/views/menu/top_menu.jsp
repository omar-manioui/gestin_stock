<nav class="navbar navbar-default " role="navigation" style="margin-bottom: 0">
            <a class="sidebar-nav" href="#"><img alt="logo" src="<%=request.getContextPath() %>/resources/logo.png" width="220px" height="55px" style="margin-top: 10px;margin-left: 10px;position: absolute; z-index: 3;" ></a>

            <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                </div>
            <ul class="nav navbar-top-links navbar-right">
                
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <c:if test="${stckAlertes.size() > 0 }">
                    <span class="fa-stack" data-count="${stckAlertes.size()}">
					  <i class="fa fa-bell fa-fw"></i><i class="fa fa-caret-down"></i>
					</span>
					</c:if>
					<c:if test="${stckAlertes.size() == 0 }">
					  <i class="fa fa-bell fa-fw"></i><i class="fa fa-caret-down"></i>
					</c:if>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                    	
                        <c:forEach items="${stckAlertes}" var="stckAlerte" end="2">
                        <li class="divider"></li>
                        <li>
                        <c:url value="/article/details/${stckAlerte.getIdArticle() }" var="alertesURL" />
                            <a href="${alertesURL }">
                                <div>
                                    <i class="fa fa-warning fa-fw"></i>  Le stock actuel de <b>${stckAlerte.getCodeArticle() }</b> est de <b>${stckAlerte.getStock() }</b> Unit�s seulement !

                                </div>
                            </a>
                        </li>
                        </c:forEach>
                        <li class="divider"></li>
                        <li>
                        <c:url value="/alertes/" var="alertesURL" />
                        
                            <a class="text-center" href="${alertesURL }">
                                <strong>Affichier tous les Notification</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                    <c:url value="/user" var="userURL" />
                        <li><a href="${userURL }"><i class="fa fa-user fa-fw"></i> Profil</a>
                        </li>
                        <li class="divider"></li>
                        <c:url value="/j_spring_security_logout" var="logout"/>
                        <li><a href="${logout }"><i class="fa fa-sign-out fa-fw"></i> Se D�connecter</a>
                        </li>
                    </ul>
                </li>
            </ul>