<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>
    <div id="wrapper">
 <%@ include file="/WEB-INF/views/menu/top_menu.jsp" %> 
  <%@ include file="/WEB-INF/views/menu/left_menu.jsp" %> 
        <div id="page-wrapper">
        <div class="row alert alert-danger alert-dismissable center">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Impossible! La Quantit� en stock est inf�rieur � la quantit� demand�e !
          </div>
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Consommations Agents</h1>
                </div>
            </div>
            	<c:url value="/commande_client/save/" var ="urlEnregistrer" />
				<f:form modelAttribute="cmdClient" action="${urlEnregistrer }"  role="form">
				<div class="row">
					<div class="form-row">
						<div class="form-group col-md-4">
							<label>Agent</label>
                   			<f:select class="form-control" path="client.idClient">
                   				<f:option value="" label="- S�lectionner un Agent -" />
                   				 <c:forEach items="${clients}" var="client">
                   				 <f:option value="${client.idClient}" label="${client.getNom()} ${client.getPrenom()}"/>
                   				 </c:forEach>
                   			</f:select>
						</div>
						<div class="form-group col-md-3"></div>
						<div class="form-group col-md-4">
							<label>Date</label>
							
							<div class="input-group date datepicker" data-date-format="mm/dd/yyyy">
		                    <f:input path="dateCommande" type='text' class="form-control" readonly="true"/>
		                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>
						</div>
					</div>
            </div>
        <div class="row">
                <div class="col-lg-12">
                   
                        <div class="table-responsive">
                            <table width="100%" class="table">
                                <thead>
                                    <tr>
                                    	<th>Article</th>
                                        <th>Commentaire</th>
                                        <th>Quantit�</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                
                                <tbody id="myTbody">
                                <tr>
									<td>
										<f:select class="form-control"  path="ligneCommandeClients[0].article.idArticle">
											<f:option value="" label="- S�lectionner un Article -" />
							 				<f:options items="${articles }" itemLabel="codeArticle" itemValue="idArticle"/>
										</f:select>
									</td>
									<td>
									<f:input path="ligneCommandeClients[0].commentaire" class="form-control" placeholder="Commentaire" />
									</td>
									<td>
									<f:input class="numero form-control" path="ligneCommandeClients[0].qte" placeholder="Quantite" />
									</td>
									<td>
									</td>
								</tr>
								</tbody>
								<tfoot>
								  <tr>
								  <th></th>
								  <th></th>
								  <th></th>
								    <th colspan="5">
								    <span type="button" onclick="CreateRow()" class="btn btn-info"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Ajouter&nbsp;</span>
								    </th>
								  </tr>
								</tfoot>
                            </table>

                        </div>
                </div>

            </div>
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Enregister</button>
            </f:form>
        </div>
    </div>
<script>
function CreateRow() {

	var x=document.getElementById('myTbody');
    var len = x.rows.length;
    if(len<15){
    	var new_row = x.rows[0].cloneNode(true);
    var inp0 = new_row.cells[0].getElementsByTagName('select')[0];
    inp0.id = 'ligneCommandeClients'+len+'.article.idArticle';
    inp0.name = 'ligneCommandeClients['+len+'].article.idArticle';
    inp0.value = '';
    var inp1 = new_row.cells[1].getElementsByTagName('input')[0];
    inp1.id = 'ligneCommandeClients'+len+'.commentaire';
    inp1.name = 'ligneCommandeClients['+len+'].commentaire';
    inp1.value = '';
    var inp2 = new_row.cells[2].getElementsByTagName('input')[0];
    inp2.id = 'ligneCommandeClients'+len+'.qte';
    inp2.name = 'ligneCommandeClients['+len+'].qte';
    inp2.value = '0';
    new_row.cells[3].innerHTML = '<span type="button" onclick="deleteRow(this)" class="btn btn-danger">&nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;</span>';
    x.appendChild( new_row );
    $(".numero").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       	 		if(e.which == 45 || e.which == 46){return true;}
                  return false;
       }
      });
    }
}


function deleteRow(row) {
	var x=document.getElementById('myTbody');
	var i=row.parentNode.parentNode.rowIndex;	
	var new_row = x.rows[i-1];
	var inp0 = new_row.cells[0].getElementsByTagName('select')[0];
    inp0.value = '';
	    
	    document.getElementById('myTbody').deleteRow(i-1);
}
</script>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   