<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<body>

    <div id="wrapper">


        <!-- Page Content -->
        <div id="page-wrapper">
        <div class="row">
                <div class="page-headerpdf col-lg-12">
                <div class="col-lg-6">
                    <h1 ><c:out value="${cmd.getCode() }"/></h1>
                </div>
                </div>
            </div>

        <div class="row">        
				<div class="col-lg-6">
                        	<ul class="list-group list-info list-unstyled">

                                <li class="list-group-item" style="border-bottom: 1px solid #eee;">
                                <i class="icon fa fa-user"></i>
                                    <label>Agent </label>${cmd.getClient().getGnr() }.&nbsp;${cmd.getClient().getPrenom()}&nbsp;${cmd.getClient().getNom() }
                                </li>
                                <li style="border-bottom: 1px solid #eee;" >
                                <i class="icon fa fa-calendar"></i>
                                    <label>Date</label><fmt:formatDate value="${cmd.getDateCommande() }" pattern="dd/MM/yyyy" /> 
                                </li>
                            </ul>
                      </div>            
            </div>
             <div class="row">
                <div class="col-lg-12">
                    <h2 style="border-bottom: 1px solid #eee; color: #337ab7;">Articles Consommés</h2>
                </div>
            </div>
            <table width="100%" class="table">
                                <thead>
                                    <tr>
                                        <th>Article</th>
                                        <th>Commentaire</th>
                                        <th>Quantite</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${lcmd}" var="lcmdarticle">
                                 <tr class="danger">
                                        <td><b>${lcmdarticle.getArticle().getCodeArticle() }</b></td>
                                        <td><b>${lcmdarticle.getCommentaire() }</b></td>
                                        <td>${lcmdarticle.getQte() }</td>
                                 		
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
            

        </div>
    </div>

 <%@ include file="/WEB-INF/views/includes/js.jsp" %>   