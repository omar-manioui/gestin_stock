package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Cle implements Serializable{

	@Id
	@GeneratedValue
	private Long idCle;
	
	private String code;
	
	private String designation;
	
	private int qte;
	
	private int reste;
	
	@OneToMany(mappedBy = "cle")
	private List<CommandeKey> cmdKey;

	public Long getIdCle() {
		return idCle;
	}

	public void setIdCle(Long idCle) {
		this.idCle = idCle;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<CommandeKey> getCmdKey() {
		return cmdKey;
	}

	public void setCmdKey(List<CommandeKey> cmdKey) {
		this.cmdKey = cmdKey;
	}

	public int getQte() {
		return qte;
	}

	public void setQte(int qte) {
		this.qte = qte;
	}

	public int getReste() {
		return reste;
	}

	public void setReste(int reste) {
		this.reste = reste;
	}
	
	
	
}
