package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class CommandeKey implements Serializable{

	@Id
	@GeneratedValue
	private Long idCmdKey;
	
	private int qte;
	
	private String commentaire;
	

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@ManyToOne
	@JoinColumn(name = "idClient")
	private Client client;
	
	@ManyToOne
	@JoinColumn(name = "idCle")
	private Cle cle;

	public Long getIdCmdKey() {
		return idCmdKey;
	}

	public void setIdCmdKey(Long idCmdKey) {
		this.idCmdKey = idCmdKey;
	}

	public int getQte() {
		return qte;
	}

	public void setQte(int qte) {
		this.qte = qte;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Cle getCle() {
		return cle;
	}

	public void setCle(Cle cle) {
		this.cle = cle;
	}

	
	
}
