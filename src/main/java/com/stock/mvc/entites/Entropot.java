package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Entropot implements Serializable{

	@Id
	@GeneratedValue
	private Long idEntropot;
	
	private String code;
	
	private String designation;
	
	@OneToMany(mappedBy = "entropot")
	private List<Article> articles;
	
	public Entropot() {}
	
	public Entropot(String code, String desig) {
		this.code = code;
		this.designation = desig;
	}

	public Long getIdEntropot() {
		return idEntropot;
	}

	public void setIdEntropot(Long id) {
		this.idEntropot = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<Article> getArticles() {

		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
}
