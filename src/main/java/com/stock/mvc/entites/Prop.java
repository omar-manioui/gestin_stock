package com.stock.mvc.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "propriete")
public class Prop implements Serializable{

	@Id
	@GeneratedValue
	private Long idProp;
	
	private String codeProp;
	
	private String designation;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
		
	private int qte = 0;
	
	private int reste;
	
	private BigDecimal valeur = new BigDecimal(0);
	
	private String photo;
	
	@ManyToOne
	@JoinColumn(name = "idCategory")
	private Category category;
	
	@ManyToOne
	@JoinColumn(name = "idEntropot")
	private Entropot entropot;
	
	@OneToMany(mappedBy = "prop")
	private List<CommandeProp> cmdprop;
	
	public Prop() {
	}

	public Long getIdProp() {
		return idProp;
	}

	public void setIdProp(Long idProp) {
		this.idProp = idProp;
	}

	public String getCodeProp() {
		return codeProp;
	}

	public void setCodeProp(String codeProp) {
		this.codeProp = codeProp;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getValeur() {
		return valeur;
	}

	public void setValeur(BigDecimal valeur) {
		this.valeur = valeur;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Entropot getEntropot() {
		return entropot;
	}

	public void setEntropot(Entropot entropot) {
		this.entropot = entropot;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<CommandeProp> getCmdprop() {
		return cmdprop;
	}

	public void setCmdprop(List<CommandeProp> cmdprop) {
		this.cmdprop = cmdprop;
	}

	public int getQte() {
		return qte;
	}

	public void setQte(int qte) {
		this.qte = qte;
	}

	public int getReste() {
		return reste;
	}

	public void setReste(int reste) {
		this.reste = reste;
	}
	
	
	
}
