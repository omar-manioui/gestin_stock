package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CommandeProp implements Serializable{

	@Id
	@GeneratedValue
	private Long idCmdProp;
	
	private int qte;
	
	private String commentaire;
	

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@ManyToOne
	@JoinColumn(name = "idClient")
	private Client client;
	
	@ManyToOne
	@JoinColumn(name = "idProp")
	private Prop prop;

	public Long getIdCmdProp() {
		return idCmdProp;
	}

	public void setIdCmdProp(Long idCmdProp) {
		this.idCmdProp = idCmdProp;
	}

	public int getQte() {
		return qte;
	}

	public void setQte(int qte) {
		this.qte = qte;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Prop getProp() {
		return prop;
	}

	public void setProp(Prop prop) {
		this.prop = prop;
	}

	
	
}
