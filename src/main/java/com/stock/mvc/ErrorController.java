package com.stock.mvc;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/403")
public class ErrorController {
 
	@RequestMapping(value = "/")
	public String error() {
		
		return "errors/403";
	} 

	
	

}