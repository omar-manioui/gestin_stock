package com.stock.mvc.modules;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import com.stock.mvc.entites.Entropot;
import com.stock.mvc.services.IEntropotService;

public class EntropotModuleImpl implements IEntropotModule {
	@Autowired
	private  IEntropotService entropotService;



	@Override
	public void insertEntropots(Model model) {
List<Entropot> entropots = entropotService.selectAll();
		
		if (entropots == null) {
			entropots = new ArrayList<Entropot>();
		}
		model.addAttribute("entropots", entropots);
	}

}
