package com.stock.mvc.modules;

import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;





public class PdfModuleImpl implements IPdfModule {
	
	@Autowired
    ServletContext context;
	
	@Override
	public String getPdf(String url){
		Random rand = new Random();
		String name = "/tmp/_"+rand.nextInt(1000)+".pdf";
		String path = context.getContextPath();if(path.equals("/")) {path="";}

		Runtime run = Runtime.getRuntime();   
		
		Process p = null;  
		String cmd = "wkhtmltopdf --no-background --header-html http://localhost:8080"+path+"/header --footer-html http://localhost:8080"+path+"/footer "+url+" "+name;  
		try {  
		    p = run.exec(cmd);  
		
		    p.getErrorStream();  
		    p.waitFor();
		
		}  
		catch (IOException e) {  
		    e.printStackTrace();  
		    System.out.println("ERROR.RUNNING.CMD");  
		
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
		    p.destroy();
		}  
		

		
		return name;
	}
	

}
