package com.stock.mvc.modules;

import org.springframework.web.multipart.MultipartFile;

public interface IUploadModule {
	public String SaveFile(MultipartFile file, String name);

}
