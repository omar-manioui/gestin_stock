package com.stock.mvc.modules;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.web.multipart.MultipartFile;

public class UploadModuleImpl implements IUploadModule {
	
	
	@Override
	public String SaveFile(MultipartFile file, String name) {
		String f = null;
		 try {

	    	 // Get the file and save it somewhere
	        byte[] bytes = file.getBytes();
	        Path path = Paths.get(name+file.getOriginalFilename());
	        Files.write(path, bytes);
			f = path.getFileName().toString();
			

	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		return f;
	}

}
