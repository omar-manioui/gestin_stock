package com.stock.mvc.modules;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import com.stock.mvc.entites.Category;
import com.stock.mvc.services.ICategoryService;

public class CategoryModuleImpl implements ICategoryModule {
	@Autowired
	private  ICategoryService catService;


	@Override
	public void insertCategories(Model model) {
		List<Category> categories = catService.selectAll();
		
		if (categories == null) {
			categories = new ArrayList<Category>();
		}
		model.addAttribute("categories", categories);


	}

}
