package com.stock.mvc.dao;

import com.stock.mvc.entites.CommandeKey;

public interface ICommandeKeyDao extends IGenericDao<CommandeKey> {

}
