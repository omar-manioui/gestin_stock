package com.stock.mvc.dao;

import com.stock.mvc.entites.Programme;;

public interface IProgrammeDao extends IGenericDao<Programme> {

}
