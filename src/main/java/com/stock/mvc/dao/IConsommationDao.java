package com.stock.mvc.dao;

import com.stock.mvc.entites.Consommation;

public interface IConsommationDao extends IGenericDao<Consommation> {

}
