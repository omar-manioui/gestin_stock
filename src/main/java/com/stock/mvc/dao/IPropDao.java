package com.stock.mvc.dao;

import com.stock.mvc.entites.Prop;

public interface IPropDao extends IGenericDao<Prop> {

}
