package com.stock.mvc.dao;

import com.stock.mvc.entites.CommandeProp;

public interface ICommandePropDao extends IGenericDao<CommandeProp> {

}
