package com.stock.mvc;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.stock.mvc.alertes.IStockAlertes;
import com.stock.mvc.entites.Client;
import com.stock.mvc.entites.CommandeClient;
import com.stock.mvc.entites.LigneCommandeClient;
import com.stock.mvc.modules.ICategoryModule;
import com.stock.mvc.modules.IEntropotModule;
import com.stock.mvc.modules.IPdfModule;
import com.stock.mvc.modules.IUploadModule;
import com.stock.mvc.modules.Periode;
import com.stock.mvc.services.IClientService;
import com.stock.mvc.services.ICommandeClientService;
import com.stock.mvc.services.ILigneCommandeClientService;

@Controller
@RequestMapping(value = "/client")
public class ClientController {
	private Periode periode;
	private boolean bool=false;
	
	@Autowired
    ServletContext context;
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private ILigneCommandeClientService ligneCmdCltService;
	
	@Autowired
	private ICommandeClientService cmdCltService;
	
	@Autowired
	private  IUploadModule saveFile;
	
	@Autowired
	private  ICategoryModule catModule;
	
	@Autowired
	private  IEntropotModule entropotModule;
	
	@Autowired
	private  IPdfModule pdfModule;
	
	@Autowired
	private  IStockAlertes alerteStock;
	
	@RequestMapping(value = "/")
	public String client(Model model) {
		List<Client> clients = clientService.selectAll();
		
		if (clients == null) {
			clients = new ArrayList<Client>();
		}
		model.addAttribute("clients", clients);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "client/client";
	}
	
	@RequestMapping(value = "/details/{idClient}")
	public String listArticle(Model model, @PathVariable Long idClient) {
		Client client = clientService.getById(idClient);
		Periode periode = new Periode();
		if(client != null) {

			List<LigneCommandeClient> lcmdclns = ligneCmdCltService.selectAll();
			List<LigneCommandeClient> lcmdarticles = new ArrayList<LigneCommandeClient>();
			
			for (LigneCommandeClient lcmdcln : lcmdclns) {
				if(lcmdcln.getCommandeClient().getClient().getIdClient() == idClient) {
					lcmdarticles.add(lcmdcln);
					
				}
			}

		model.addAttribute("periode", periode);
		model.addAttribute("client", client);	
		model.addAttribute("lcmdarticles", lcmdarticles);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "client/detailClient";
		}
		return "redirect:/client/";
	}
	@RequestMapping(value = "/details_par_date/{idClient}")
	public String listArticleDate(Model model,Periode periode, @PathVariable Long idClient) {
		this.periode = periode;
		Client client = clientService.getById(idClient);
		
		if(client != null) {

			List<LigneCommandeClient> lcmdclns = ligneCmdCltService.selectAll();
			List<LigneCommandeClient> lcmdarticles = new ArrayList<LigneCommandeClient>();
			
			for (LigneCommandeClient lcmdcln : lcmdclns) {
				if(lcmdcln.getCommandeClient().getClient().getIdClient() == idClient) {
					if(lcmdcln.getCommandeClient().getDateCommande().compareTo(periode.getDe())>=0 && lcmdcln.getCommandeClient().getDateCommande().compareTo(periode.getA())<=0) {
					lcmdarticles.add(lcmdcln);}
					
				}
			}

		model.addAttribute("periode", periode);
		model.addAttribute("client", client);	
		model.addAttribute("lcmdarticles", lcmdarticles);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "client/detailClient";
		}
		return "redirect:/client/";
	}
	
	@RequestMapping(value = "/pdf/{idClient}")
	public String clientA4(Model model, @PathVariable Long idClient) {
		Client client = clientService.getById(idClient);
		
		if(client != null) {if(bool){


			List<LigneCommandeClient> lcmdclns = ligneCmdCltService.selectAll();
			List<LigneCommandeClient> lcmdarticles = new ArrayList<LigneCommandeClient>();
			
			for (LigneCommandeClient lcmdcln : lcmdclns) {
				if(lcmdcln.getCommandeClient().getClient().getIdClient() == idClient) {
					if(lcmdcln.getCommandeClient().getDateCommande().compareTo(periode.getDe())>=0 && lcmdcln.getCommandeClient().getDateCommande().compareTo(periode.getA())<=0) {
					lcmdarticles.add(lcmdcln);}
					
				}
			}
		model.addAttribute("client", client);	
		model.addAttribute("lcmdarticles", lcmdarticles);
		return "client/detailClientPdf";
		
		}else{

			List<LigneCommandeClient> lcmdclns = ligneCmdCltService.selectAll();
			List<LigneCommandeClient> lcmdarticles = new ArrayList<LigneCommandeClient>();
			
			for (LigneCommandeClient lcmdcln : lcmdclns) {
				if(lcmdcln.getCommandeClient().getClient().getIdClient() == idClient) {
					lcmdarticles.add(lcmdcln);
					
				}
			}
		model.addAttribute("client", client);	
		model.addAttribute("lcmdarticles", lcmdarticles);
		return "client/detailClientPdf";
		}}
		return "redirect:/client/";
	}
	
	
	@RequestMapping(value = "/detailspdf/{idClient}")
	public String pdf(Model model,HttpServletRequest request, @PathVariable Long idClient) {
		this.bool = request.getHeader("referer").contains("details_par_date");
		String path = context.getContextPath();if(path.equals("/")) {path="";}
		Client client = clientService.getById(idClient);
		if(client != null) {String url = "http://localhost:8080"+path+"/client/pdf/"+idClient;
			return "redirect:"+pdfModule.getPdf(url);
		}
		return "redirect:/client/";
	}
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterClient(Model model) {
		Client client = new Client();
		model.addAttribute("client", client);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "client/ajouterClient";
	}
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerClient(Model model, Client client, @RequestParam("file") MultipartFile file) {
		String path = context.getRealPath("/images/client/");
		if (client != null) {
			if (file != null && !file.isEmpty()) {
		       client.setPhoto(saveFile.SaveFile(file, path+client.getNom()+"_"+client.getPrenom()+"_"));
					}else {if(client.getPhoto().isEmpty()) {
						if(client.getGnr().equals("Mme") || client.getGnr().equals("Mlle")){client.setPhoto("avatara.png");
						}else {client.setPhoto("avatar.png");}
						}}

			
			if (client.getIdClient() != null) {
				clientService.update(client);
			} else {
				clientService.save(client);
			}
		}
		return "redirect:/client/";
	}
	
	
	@RequestMapping(value = "/modifier/{idClient}")
	public String modifierClient(Model model, @PathVariable Long idClient) {
		if (idClient != null) {
			Client client = clientService.getById(idClient);
			if (client != null) {
				model.addAttribute("client", client);
			}
		}
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "client/ajouterClient";
	}
	
	@RequestMapping(value = "/supprimer/{idClient}")
	public String supprimerClient(Model model, @PathVariable Long idClient) {
		if (idClient != null) {
			Client client = clientService.getById(idClient);
			if (client != null) {
				List<LigneCommandeClient> lcmdclns = ligneCmdCltService.selectAll();
				List<CommandeClient> cmdclns = cmdCltService.selectAll();
				
					for (LigneCommandeClient lcmdcln : lcmdclns) {
						if(lcmdcln.getCommandeClient().getClient().getIdClient() == idClient) {
							ligneCmdCltService.remove(lcmdcln.getIdLigneCdeClt());
							
						}}
					for (CommandeClient cmdcln : cmdclns) {
						if(cmdcln.getClient().getIdClient() == idClient) {
							cmdCltService.remove(cmdcln.getIdCommandeClient());
						}
					}
				clientService.remove(idClient);
			}
		}
		return "redirect:/client/";
	}
	
	

}
