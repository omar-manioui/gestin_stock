package com.stock.mvc;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.alertes.IStockAlertes;
import com.stock.mvc.entites.Article;
import com.stock.mvc.entites.CommandeFournisseur;
import com.stock.mvc.entites.Fournisseur;
import com.stock.mvc.entites.LigneCommandeFournisseur;
import com.stock.mvc.modules.ICategoryModule;
import com.stock.mvc.modules.IEntropotModule;
import com.stock.mvc.modules.IPdfModule;
import com.stock.mvc.services.IArticleService;
import com.stock.mvc.services.ICommandeFournisseurService;
import com.stock.mvc.services.IFournisseurService;
import com.stock.mvc.services.ILigneCommandeFournisseurService;

@Controller
@RequestMapping(value = "/commande_fournisseur")
public class CommandeFournisseurController {
	@Autowired
    ServletContext context;
	
	@Autowired
	private IFournisseurService fournisseurService;
	
	@Autowired
	private ICommandeFournisseurService cmdFrsService;
	
	@Autowired
	private ILigneCommandeFournisseurService ligneCmdFrsService;
	
	@Autowired
	private IArticleService articleService;
	
	@Autowired
	private  ICategoryModule catModule;
	
	@Autowired
	private  IEntropotModule entropotModule;
	
	@Autowired
	private  IStockAlertes alerteStock;
	
	@Autowired
	private  IPdfModule pdfModule;
	
	@RequestMapping(value = "/")
	public String commandes(Model model) {
		List<CommandeFournisseur> cmds = cmdFrsService.selectAll();
		
		if (cmds == null) {
			cmds = new ArrayList<CommandeFournisseur>();
		}
		model.addAttribute("cmds", cmds);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "commande_fournisseur/commandes";
	}
	
	@RequestMapping(value = "/details/{idCommande}")
	public String DetailCmdd(Model model, @PathVariable Long idCommande) {
		CommandeFournisseur cmd = cmdFrsService.getById(idCommande);
		if(cmd != null) {

			List<LigneCommandeFournisseur> lcmdclns = ligneCmdFrsService.selectAll();
			List<LigneCommandeFournisseur> lcmd = new ArrayList<LigneCommandeFournisseur>();
			
			for (LigneCommandeFournisseur lcmdcln : lcmdclns) {
				if(lcmdcln.getCommandeFournisseur().getIdCommandeFournisseur() == idCommande) {
					lcmd.add(lcmdcln);
					
				}
			}

		model.addAttribute("cmd", cmd);	
		model.addAttribute("lcmd", lcmd);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "commande_fournisseur/detailCommande";
		}
		return "redirect:/commande_fournisseur/";
	}
	
	@RequestMapping(value = "/pdf/{idCommande}")
	public String CommandeA4(Model model, @PathVariable Long idCommande) {
		CommandeFournisseur cmd = cmdFrsService.getById(idCommande);
		
		if(cmd != null) {

			List<LigneCommandeFournisseur> lcmdclns = ligneCmdFrsService.selectAll();
			List<LigneCommandeFournisseur> lcmd = new ArrayList<LigneCommandeFournisseur>();
			
			for (LigneCommandeFournisseur lcmdcln : lcmdclns) {
				if(lcmdcln.getCommandeFournisseur().getIdCommandeFournisseur() == idCommande) {
					lcmd.add(lcmdcln);
					
				}
			}

		model.addAttribute("cmd", cmd);	
		model.addAttribute("lcmd", lcmd);
		return "commande_fournisseur/detailCommandePdf";
		}
		return "redirect:/commande_fournisseur/";
	}
	
	@RequestMapping(value = "/detailspdf/{idCommande}")
	public String pdf(Model model, @PathVariable Long idCommande) {
		CommandeFournisseur cmd = cmdFrsService.getById(idCommande);
		String path = context.getContextPath();if(path.equals("/")) {path="";}
		if(cmd != null) {String url = "http://localhost:8080"+path+"/commande_fournisseur/pdf/"+idCommande;
			return "redirect:"+pdfModule.getPdf(url);
		}
		return "redirect:/commande_fournisseur/";
	}
	
	@RequestMapping(value = "/nouveau")
	public String fournisseur(Model model) {
		CommandeFournisseur cmdFournisseur = new CommandeFournisseur();
		cmdFournisseur.setLigneCommandeFournisseurs(new ArrayList<LigneCommandeFournisseur>());
		
		for(int i=0;i<15;i++) {
			cmdFournisseur.getLigneCommandeFournisseurs().add(new LigneCommandeFournisseur());
		}
		
		List<Fournisseur> fournisseurs = fournisseurService.selectAll();
		List<Article> articles = articleService.selectAll();
		
		if (fournisseurs == null) {
			fournisseurs = new ArrayList<Fournisseur>();
		}
		if (articles == null) {
			articles = new ArrayList<Article>();
		}

		model.addAttribute("cmdFournisseur", cmdFournisseur);
		model.addAttribute("fournisseurs", fournisseurs);
		model.addAttribute("articles", articles);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "commande_fournisseur/commande_fournisseur";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String enregistrerCommandFournisseur(Model model, CommandeFournisseur cmdFournisseur) {
		if(cmdFournisseur != null) {
			if (cmdFournisseur.getIdCommandeFournisseur() != null) {
				cmdFrsService.update(cmdFournisseur);
			} else {if(cmdFournisseur.getFournisseur().getIdFournisseur()!=null){
				cmdFrsService.save(cmdFournisseur);
				SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yy");
				cmdFournisseur.getDateCommande().setTime(new Date().getTime());
				cmdFournisseur.setCode("BC_"+cmdFournisseur.getIdCommandeFournisseur().toString()+"_"+formater.format(cmdFournisseur.getDateCommande()));
				cmdFrsService.update(cmdFournisseur);
				List<LigneCommandeFournisseur> lcmdFournisseurs = cmdFournisseur.getLigneCommandeFournisseurs();
				for (LigneCommandeFournisseur lcmdFournisseur : lcmdFournisseurs) {
					if(lcmdFournisseur != null) {
						if(lcmdFournisseur.getArticle() != null) {
							lcmdFournisseur.setCommandeFournisseur(cmdFournisseur);
							ligneCmdFrsService.save(lcmdFournisseur);
							Article article = articleService.getById(lcmdFournisseur.getArticle().getIdArticle());
							article.setStock(article.getStock()+lcmdFournisseur.getQte());
							articleService.update(article);
							}
						}
				}}
			}
		}
		return "redirect:/commande_fournisseur/";
	}
	
	@RequestMapping(value = "/supprimer/{idCommande}")
	public String supprimerCommande(Model model, @PathVariable Long idCommande) {
		if (idCommande != null) {
			CommandeFournisseur cmd = cmdFrsService.getById(idCommande);
			if (cmd != null) {

				List<LigneCommandeFournisseur> lcmdfrs = ligneCmdFrsService.selectAll();
				
					for (LigneCommandeFournisseur lcmdfr : lcmdfrs) {
						if(lcmdfr.getCommandeFournisseur().getIdCommandeFournisseur() == idCommande) {
							ligneCmdFrsService.remove(lcmdfr.getIdLigneCdeFrs());
							
						}}
					cmdFrsService.remove(idCommande);
			}
		}
		return "redirect:/commande_fournisseur/";
	}
	
}
