package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.Entropot;

public interface IEntropotService {
	
	public Entropot save(Entropot entity);
	
	public Entropot update(Entropot entity);

	public List<Entropot> selectAll();

	public List<Entropot> selectAll(String sortField, String sort);

	public Entropot getById(Long id);

	public void remove(Long id);

	public Entropot findOne(String paramName, Object paramValue);

	public Entropot findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
