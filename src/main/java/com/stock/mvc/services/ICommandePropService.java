package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.CommandeProp;

public interface ICommandePropService {
	
	public CommandeProp save(CommandeProp entity);
	
	public CommandeProp update(CommandeProp entity);

	public List<CommandeProp> selectAll();

	public List<CommandeProp> selectAll(String sortField, String sort);

	public CommandeProp getById(Long id);

	public void remove(Long id);

	public CommandeProp findOne(String paramName, Object paramValue);

	public CommandeProp findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
