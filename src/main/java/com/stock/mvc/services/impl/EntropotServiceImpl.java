package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IEntropotDao;
import com.stock.mvc.entites.Entropot;
import com.stock.mvc.services.IEntropotService;

@Transactional
public class EntropotServiceImpl implements IEntropotService {
	
	private IEntropotDao dao;
	
	public void setDao(IEntropotDao dao) {
		this.dao = dao;
	}

	@Override
	public Entropot save(Entropot entity) {
		return dao.save(entity);
	}

	@Override
	public Entropot update(Entropot entity) {
		return dao.update(entity);
	}

	@Override
	public List<Entropot> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Entropot> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Entropot getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Entropot findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Entropot findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
