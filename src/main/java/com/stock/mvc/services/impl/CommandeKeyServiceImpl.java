package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ICommandeKeyDao;
import com.stock.mvc.entites.CommandeKey;
import com.stock.mvc.services.ICommandeKeyService;

@Transactional
public class CommandeKeyServiceImpl implements ICommandeKeyService {
	
	private ICommandeKeyDao dao;
	
	public void setDao(ICommandeKeyDao dao) {
		this.dao = dao;
	}

	@Override
	public CommandeKey save(CommandeKey entity) {
		return dao.save(entity);
	}

	@Override
	public CommandeKey update(CommandeKey entity) {
		return dao.update(entity);
	}

	@Override
	public List<CommandeKey> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<CommandeKey> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public CommandeKey getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public CommandeKey findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeKey findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
