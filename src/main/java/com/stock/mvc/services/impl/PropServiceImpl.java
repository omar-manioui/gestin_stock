package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IPropDao;
import com.stock.mvc.entites.Prop;
import com.stock.mvc.services.IPropService;

@Transactional
public class PropServiceImpl implements IPropService {
	
	private IPropDao dao;
	
	public void setDao(IPropDao dao) {
		this.dao = dao;
	}

	@Override
	public Prop save(Prop entity) {
		return dao.save(entity);
	}

	@Override
	public Prop update(Prop entity) {
		return dao.update(entity);
	}

	@Override
	public List<Prop> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Prop> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Prop getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Prop findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Prop findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
