package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ICommandePropDao;
import com.stock.mvc.entites.CommandeProp;
import com.stock.mvc.services.ICommandePropService;

@Transactional
public class CommandePropServiceImpl implements ICommandePropService {
	
	private ICommandePropDao dao;
	
	public void setDao(ICommandePropDao dao) {
		this.dao = dao;
	}

	@Override
	public CommandeProp save(CommandeProp entity) {
		return dao.save(entity);
	}

	@Override
	public CommandeProp update(CommandeProp entity) {
		return dao.update(entity);
	}

	@Override
	public List<CommandeProp> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<CommandeProp> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public CommandeProp getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public CommandeProp findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeProp findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
