package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.Cle;

public interface IKeyService {
	
	public Cle save(Cle entity);
	
	public Cle update(Cle entity);

	public List<Cle> selectAll();

	public List<Cle> selectAll(String sortField, String sort);

	public Cle getById(Long id);

	public void remove(Long id);

	public Cle findOne(String paramName, Object paramValue);

	public Cle findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
