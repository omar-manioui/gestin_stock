package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.Consommation;

public interface IConsommationService {
	
	public Consommation save(Consommation entity);
	
	public Consommation update(Consommation entity);

	public List<Consommation> selectAll();

	public List<Consommation> selectAll(String sortField, String sort);

	public Consommation getById(Long id);

	public void remove(Long id);

	public Consommation findOne(String paramName, Object paramValue);

	public Consommation findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
