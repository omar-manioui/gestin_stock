package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.Programme;

public interface IProgrammeService {
	
	public Programme save(Programme entity);
	
	public Programme update(Programme entity);

	public List<Programme> selectAll();

	public List<Programme> selectAll(String sortField, String sort);

	public Programme getById(Long id);

	public void remove(Long id);

	public Programme findOne(String paramName, Object paramValue);

	public Programme findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
