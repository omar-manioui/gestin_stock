package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.Prop;

public interface IPropService {
	
	public Prop save(Prop entity);
	
	public Prop update(Prop entity);

	public List<Prop> selectAll();

	public List<Prop> selectAll(String sortField, String sort);

	public Prop getById(Long id);

	public void remove(Long id);

	public Prop findOne(String paramName, Object paramValue);

	public Prop findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
