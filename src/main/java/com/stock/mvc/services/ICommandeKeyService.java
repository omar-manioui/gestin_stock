package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.CommandeKey;

public interface ICommandeKeyService {
	
	public CommandeKey save(CommandeKey entity);
	
	public CommandeKey update(CommandeKey entity);

	public List<CommandeKey> selectAll();

	public List<CommandeKey> selectAll(String sortField, String sort);

	public CommandeKey getById(Long id);

	public void remove(Long id);

	public CommandeKey findOne(String paramName, Object paramValue);

	public CommandeKey findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
