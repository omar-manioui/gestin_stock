package com.stock.mvc;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.stock.mvc.alertes.IStockAlertes;
import com.stock.mvc.entites.Article;
import com.stock.mvc.entites.Category;
import com.stock.mvc.entites.Entropot;
import com.stock.mvc.entites.LigneCommandeClient;
import com.stock.mvc.entites.LigneCommandeFournisseur;
import com.stock.mvc.entites.MvtStk;
import com.stock.mvc.modules.ICategoryModule;
import com.stock.mvc.modules.IEntropotModule;
import com.stock.mvc.modules.IPdfModule;
import com.stock.mvc.modules.IUploadModule;
import com.stock.mvc.services.IArticleService;
import com.stock.mvc.services.ICategoryService;
import com.stock.mvc.services.IEntropotService;
import com.stock.mvc.services.ILigneCommandeClientService;
import com.stock.mvc.services.ILigneCommandeFournisseurService;
import com.stock.mvc.services.IMvtStkService;

@Controller
@RequestMapping(value = "/article")
public class ArticleController {
	
	@Autowired
    ServletContext context;
	
	@Autowired
	private IArticleService articleService;
	
	@Autowired
	private  IUploadModule saveFile;
	
	@Autowired
	private  ICategoryService catService;
	
	@Autowired
	private  IMvtStkService mvtStkService;
	
	@Autowired
	private  IEntropotService entropotService;
	
	@Autowired
	private  ILigneCommandeClientService ligneCmdCltService;
	
	@Autowired
	private  ICategoryModule catModule;
	
	@Autowired
	private  IEntropotModule entropotModule;
	
	@Autowired
	private  IPdfModule pdfModule;
	
	@Autowired
	private ILigneCommandeFournisseurService ligneCmdFrsService;
	
	@Autowired
	private  IStockAlertes alerteStock;
	
	@RequestMapping(value = "/")
	public String article(Model model) {
		List<Article> articles = articleService.selectAll();
		
		if (articles == null) {
			articles = new ArrayList<Article>();
		}
		model.addAttribute("articles", articles);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "article/article";
	}
	
	@RequestMapping(value = "/details/{idArticle}")
	public String listArticle(Model model, @PathVariable Long idArticle) {
		Article article = articleService.getById(idArticle);
		
		if(article != null) {

			List<LigneCommandeClient> lcmdclns = ligneCmdCltService.selectAll();
			List<LigneCommandeFournisseur> lcmdfrs = ligneCmdFrsService.selectAll();
			List<MvtStk> mvtstks = mvtStkService.selectAll();
			List<LigneCommandeClient> lcmdarticles = new ArrayList<LigneCommandeClient>();
			List<LigneCommandeFournisseur> lcmdarticlesf = new ArrayList<LigneCommandeFournisseur>();
			List<MvtStk> mvtstksarticle = new ArrayList<MvtStk>();
			
			for (LigneCommandeClient lcmdcln : lcmdclns) {
				if(lcmdcln.getArticle().getIdArticle() == idArticle) {
					lcmdarticles.add(lcmdcln);
				}
			}for (LigneCommandeFournisseur lcmdfr : lcmdfrs) {
				if(lcmdfr.getArticle().getIdArticle() == idArticle) {
					lcmdarticlesf.add(lcmdfr);
					
				}
			}for (MvtStk mvtStk : mvtstks) {
				if(mvtStk.getArticle().getIdArticle() == idArticle) {
					mvtstksarticle.add(mvtStk);
				}
				
			}

		model.addAttribute("article", article);	
		model.addAttribute("lcmdarticles", lcmdarticles);
		model.addAttribute("lcmdarticlesf", lcmdarticlesf);
		model.addAttribute("mvtstksarticle", mvtstksarticle);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "article/detailArticles";
		}
		return "redirect:/article/";
	}
	
	@RequestMapping(value = "/pdf/{idArticle}")
	public String ArticleA4(Model model, @PathVariable Long idArticle) {
		Article article = articleService.getById(idArticle);
		
		if(article != null) {

			List<LigneCommandeClient> lcmdclns = ligneCmdCltService.selectAll();
			List<LigneCommandeFournisseur> lcmdfrs = ligneCmdFrsService.selectAll();
			List<MvtStk> mvtstks = mvtStkService.selectAll();
			List<LigneCommandeClient> lcmdarticles = new ArrayList<LigneCommandeClient>();
			List<LigneCommandeFournisseur> lcmdarticlesf = new ArrayList<LigneCommandeFournisseur>();
			List<MvtStk> mvtstksarticle = new ArrayList<MvtStk>();
			
			for (LigneCommandeClient lcmdcln : lcmdclns) {
				if(lcmdcln.getArticle().getIdArticle() == idArticle) {
					lcmdarticles.add(lcmdcln);
				}
			}for (LigneCommandeFournisseur lcmdfr : lcmdfrs) {
				if(lcmdfr.getArticle().getIdArticle() == idArticle) {
					lcmdarticlesf.add(lcmdfr);
					
				}
			}for (MvtStk mvtStk : mvtstks) {
				if(mvtStk.getArticle().getIdArticle() == idArticle) {
					mvtstksarticle.add(mvtStk);
				}
				
			}

		model.addAttribute("article", article);	
		model.addAttribute("lcmdarticles", lcmdarticles);
		model.addAttribute("lcmdarticlesf", lcmdarticlesf);
		model.addAttribute("mvtstksarticle", mvtstksarticle);
		return "article/detailArticlePdf";
		}
		return "redirect:/article/";
	}
	
	@RequestMapping(value = "/detailspdf/{idArticle}")
	public String pdf(Model model, @PathVariable Long idArticle) {
		Article article = articleService.getById(idArticle);
		String path = context.getContextPath();if(path.equals("/")) {path="";}
		if(article != null) {String url = "http://localhost:8080"+path+"/article/pdf/"+idArticle;
			return "redirect:"+pdfModule.getPdf(url);
		}
		return "redirect:/article/";
	}
	
	@RequestMapping(value = "/category/{idCategory}")
	public String articleByCategory(Model model, @PathVariable Long idCategory) {
		List<Article> articles = articleService.selectAll();
		List<Article> articlesbycat = new ArrayList<Article>();
		if (articles == null) {
			articles = new ArrayList<Article>();
		}
		for (Article article : articles) {
			if (article.getCategory().getIdCategory() == idCategory) {
				articlesbycat.add(article);
			}
		}
		model.addAttribute("articles", articlesbycat);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "article/article";
	}
	
	@RequestMapping(value = "/entropot/{idEntropot}")
	public String articleByEntropot(Model model, @PathVariable Long idEntropot) {
		List<Article> articles = articleService.selectAll();
		List<Article> articlesbyentropot = new ArrayList<Article>();
		if (articles == null) {
			articles = new ArrayList<Article>();
		}
		for (Article article : articles) {
			if (article.getEntropot().getIdEntropot() == idEntropot) {
				articlesbyentropot.add(article);
			}
		}
		model.addAttribute("articles", articlesbyentropot);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "article/article";
	}
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterArticle(Model model) {
		Article article = new Article();
		List<Category> categories = catService.selectAll();
		List<Entropot> entropots = entropotService.selectAll();
		if (categories == null) {
			categories = new ArrayList<Category>();
			
		}if(categories.isEmpty()) {
			Category cat = new Category("Tous","Tous");
			catService.save(cat);
			categories.add(cat);
			
		}
		if (entropots == null) {
			entropots = new ArrayList<Entropot>();
			
		}if(entropots.isEmpty()) {
			Entropot entro = new Entropot("FM6OA","FM6OA");
			entropotService.save(entro);
			entropots.add(entro);
			
		}
		
		model.addAttribute("article", article);
		model.addAttribute("categories", categories);
		model.addAttribute("entropots", entropots);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "article/ajouterArticle";
	}
	

	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerArticle(Model model, Article article, @RequestParam("file") MultipartFile file) {

		String path = context.getRealPath("/images/article/");
		if (article != null) {
			if (file != null && !file.isEmpty()) {
				article.setPhoto(saveFile.SaveFile(file, path+article.getCodeArticle()+"_"));
					}else {if(article.getPhoto().isEmpty()) {article.setPhoto("avatar.png");}}

			
			if (article.getIdArticle() != null) {
				if(articleService.getById(article.getIdArticle()).getStock()-article.getStock()!=0) {
				MvtStk mvt = new MvtStk();
				mvt.setArticle(article);
				mvt.setDateMvt(new Date());
				mvt.setCommentaire("Mise a jour du Stock !");
				mvt.setQuantite(article.getStock()-articleService.getById(article.getIdArticle()).getStock());
				mvtStkService.save(mvt);}
				articleService.update(article);
				
			} else {
				articleService.save(article);
				MvtStk mvt = new MvtStk();
				mvt.setArticle(article);
				mvt.setDateMvt(new Date());
				mvt.setCommentaire("Stock initial!");
				mvt.setQuantite(article.getStock());
				mvtStkService.save(mvt);
			}
		}
		return "redirect:/article/";
	}
	
	
	@RequestMapping(value = "/modifier/{idArticle}")
	public String modifierArticle(Model model, @PathVariable Long idArticle) {
		if (idArticle != null) {
			Article article = articleService.getById(idArticle);
			if (article != null) {
				model.addAttribute("article", article);
				
			}
		}
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "article/ajouterArticle";
	}
	
	@RequestMapping(value = "/supprimer/{idArticle}")
	public String supprimerFournisseur(Model model, @PathVariable Long idArticle) {
		if (idArticle != null) {
			Article article = articleService.getById(idArticle);
			if (article != null) {
				
				List<LigneCommandeClient> lcmdclns = ligneCmdCltService.selectAll();
				List<LigneCommandeFournisseur> lcmdfrs = ligneCmdFrsService.selectAll();
				
					for (LigneCommandeClient lcmdcln : lcmdclns) {
						if(lcmdcln.getArticle().getIdArticle() == idArticle) {
							ligneCmdCltService.remove(lcmdcln.getIdLigneCdeClt());
						}
					}for (LigneCommandeFournisseur lcmdfr : lcmdfrs) {
						if(lcmdfr.getArticle().getIdArticle() == idArticle) {
							ligneCmdFrsService.remove(lcmdfr.getIdLigneCdeFrs());
							
						}}
					articleService.remove(idArticle);
			}
		}
		return "redirect:/article/";
	}
	
	

}
