package com.stock.mvc.alertes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import com.stock.mvc.entites.Article;
import com.stock.mvc.services.IArticleService;

public class AlertesImpl implements IStockAlertes {

	@Autowired
	private IArticleService articleService;
	
	@Override
	public List<Article> alertes() {
		List<Article> articles = articleService.selectAll();
		List<Article> articlesalerted = new ArrayList<Article>();
		for (Article article : articles) {
			if (article.getStockMin()  >= article.getStock()) {
				articlesalerted.add(article);
			}
		}
		return articlesalerted;	
	}

	@Override
	public void insertAlertes(Model model, List<Article> articles) {
		if (articles == null) {
			articles = new ArrayList<Article>();
		}
		model.addAttribute("stckAlertes", articles);
	}


}
