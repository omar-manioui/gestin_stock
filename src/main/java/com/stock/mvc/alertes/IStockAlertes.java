package com.stock.mvc.alertes;

import java.util.List;

import org.springframework.ui.Model;

import com.stock.mvc.entites.Article;

public interface IStockAlertes {
	public List<Article> alertes();
	public void insertAlertes(Model model, List<Article> articles);
	
}
