package com.stock.mvc;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.alertes.IStockAlertes;
import com.stock.mvc.entites.Consommation;
import com.stock.mvc.modules.ICategoryModule;
import com.stock.mvc.modules.IEntropotModule;
import com.stock.mvc.modules.IPdfModule;
import com.stock.mvc.services.IConsommationService;

@Controller
@RequestMapping(value = "/consommation")
public class ConsommationController {
	@Autowired
    ServletContext context;
	
	@Autowired
	private  IConsommationService consommationService;
	
	@Autowired
	private  ICategoryModule catModule;
	
	@Autowired
	private  IEntropotModule entropotModule;
	
	@Autowired
	private  IStockAlertes alerteStock;
	
	@Autowired
	private  IPdfModule pdfModule;
	
	@RequestMapping(value = "/")
	public String consommation(Model model) {
		List<Consommation> consos = consommationService.selectAll();
		if(consos == null) {
			consos = new ArrayList<Consommation>();
		}
		model.addAttribute("consos", consos);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "consommation/consommation";
	}
	
	@RequestMapping(value = "/details/{idConsommation}")
	public String detailsConsommation(Model model, @PathVariable Long idConsommation) {
		Consommation conso = consommationService.getById(idConsommation);
		
		if(conso != null) {

		model.addAttribute("conso", conso);	
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "consommation/detailConsommation";
		}
		return "redirect:/consommation/";
	}
	
	@RequestMapping(value = "/pdf/{idConsommation}")
	public String ArticleA4(Model model, @PathVariable Long idConsommation) {
		Consommation conso = consommationService.getById(idConsommation);
		
		if(conso != null) {
			
		model.addAttribute("conso", conso);	
		return "consommation/detailConsommationPdf";
		}
		return "redirect:/consommation/";
	}
	
	@RequestMapping(value = "/detailspdf/{idConsommation}")
	public String pdf(Model model, @PathVariable Long idConsommation) {
		Consommation conso = consommationService.getById(idConsommation);
		String path = context.getContextPath();if(path.equals("/")) {path="";}
		if(conso != null) {String url = "http://localhost:8080"+path+"/consommation/pdf/"+idConsommation;
			return "redirect:"+pdfModule.getPdf(url);
		}
		return "redirect:/consommation/";
	}
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterConsommation(Model model) {
		Consommation conso = new Consommation();
		model.addAttribute("conso", conso);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "consommation/new_consommation";
	}
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerConsommation(Model model, Consommation conso) {
		
		if (conso != null) {
			
			
			if (conso.getIdConsommation() != null) {
				consommationService.update(conso);
			} else {
				consommationService.save(conso);
			}
		}
		return "redirect:/consommation/";
	}
	
	
	@RequestMapping(value = "/modifier/{idConsommation}")
	public String modifierConsommation(Model model, @PathVariable Long idConsommation) {
		if (idConsommation != null) {
			Consommation conso = consommationService.getById(idConsommation);
			if (conso != null) {
				model.addAttribute("conso", conso);
			}
		}
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "consommation/new_consommation";
	}
	
	@RequestMapping(value = "/supprimer/{idConsommation}")
	public String supprimerConsommation(Model model, @PathVariable Long idConsommation) {
		if (idConsommation != null) {
			Consommation conso = consommationService.getById(idConsommation);
			if (conso != null) {
				//TODO Verification avant suppression
				consommationService.remove(idConsommation);
			}
		}
		return "redirect:/consommation/";
	}
	
	

}
