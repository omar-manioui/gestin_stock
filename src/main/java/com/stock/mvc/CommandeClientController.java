package com.stock.mvc;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.alertes.IStockAlertes;
import com.stock.mvc.entites.Article;
import com.stock.mvc.entites.Client;
import com.stock.mvc.entites.CommandeClient;
import com.stock.mvc.entites.LigneCommandeClient;
import com.stock.mvc.modules.ICategoryModule;
import com.stock.mvc.modules.IEntropotModule;
import com.stock.mvc.modules.IPdfModule;
import com.stock.mvc.services.IArticleService;
import com.stock.mvc.services.IClientService;
import com.stock.mvc.services.ICommandeClientService;
import com.stock.mvc.services.ILigneCommandeClientService;

@Controller
@RequestMapping(value = "/commande_client")
public class CommandeClientController {
	@Autowired
    ServletContext context;
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private ICommandeClientService cmdCltService;
	
	@Autowired
	private ILigneCommandeClientService ligneCmdCltService;
	
	@Autowired
	private IArticleService articleService;
	
	@Autowired
	private  IEntropotModule entropotModule;
	
	@Autowired
	private  ICategoryModule catModule;
	
	@Autowired
	private  IStockAlertes alerteStock;
	
	@Autowired
	private  IPdfModule pdfModule;
	
	@RequestMapping(value = "/")
	public String commandes(Model model) {
		
		List<CommandeClient> cmds = cmdCltService.selectAll();
		
		if (cmds == null) {
			cmds = new ArrayList<CommandeClient>();
		}
		model.addAttribute("cmds", cmds);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "commande_client/commandes";
	}
	
	@RequestMapping(value = "/details/{idCommande}")
	public String DetailCmdd(Model model, @PathVariable Long idCommande) {
		CommandeClient cmd = cmdCltService.getById(idCommande);
		if(cmd != null) {

			List<LigneCommandeClient> lcmdclns = ligneCmdCltService.selectAll();
			List<LigneCommandeClient> lcmd = new ArrayList<LigneCommandeClient>();
			
			for (LigneCommandeClient lcmdcln : lcmdclns) {
				if(lcmdcln.getCommandeClient().getIdCommandeClient() == idCommande) {
					lcmd.add(lcmdcln);
					
				}
			}

		model.addAttribute("cmd", cmd);	
		model.addAttribute("lcmd", lcmd);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "commande_client/detailCommande";
		}
		return "redirect:/commande_client/";
	}
	
	@RequestMapping(value = "/pdf/{idCommande}")
	public String CommandeA4(Model model, @PathVariable Long idCommande) {
		CommandeClient cmd = cmdCltService.getById(idCommande);
		
		if(cmd != null) {

			List<LigneCommandeClient> lcmdclns = ligneCmdCltService.selectAll();
			List<LigneCommandeClient> lcmd = new ArrayList<LigneCommandeClient>();
			
			for (LigneCommandeClient lcmdcln : lcmdclns) {
				if(lcmdcln.getCommandeClient().getIdCommandeClient() == idCommande) {
					lcmd.add(lcmdcln);
					
				}
			}

		model.addAttribute("cmd", cmd);	
		model.addAttribute("lcmd", lcmd);
		return "commande_client/detailCommandePdf";
		}
		return "redirect:/commande_client/";
	}
	
	@RequestMapping(value = "/detailspdf/{idCommande}")
	public String pdf(Model model, @PathVariable Long idCommande) {
		CommandeClient cmd = cmdCltService.getById(idCommande);
		String path = context.getContextPath();if(path.equals("/")) {path="";}
		if(cmd != null) {String url = "http://localhost:8080"+path+"/commande_client/pdf/"+idCommande;
			return "redirect:"+pdfModule.getPdf(url);
		}
		return "redirect:/commande_client/";
	}
	
	@RequestMapping(value = "/nouveau")
	public String client(Model model) {
		CommandeClient cmdClient = new CommandeClient();
		cmdClient.setLigneCommandeClients(new ArrayList<LigneCommandeClient>());
		
		for(int i=0;i<15;i++) {
			cmdClient.getLigneCommandeClients().add(new LigneCommandeClient());
		}
		
		List<Client> clients = clientService.selectAll();
		List<Article> articles = articleService.selectAll();
		
		if (clients == null) {
			clients = new ArrayList<Client>();
		}
		if (articles == null) {
			articles = new ArrayList<Article>();
		}

		model.addAttribute("cmdClient", cmdClient);
		model.addAttribute("clients", clients);
		model.addAttribute("articles", articles);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "commande_client/commande_client";
	}
	
	@RequestMapping(value = "/ereurQuantite")
	public String quatite(Model model) {
		CommandeClient cmdClient = new CommandeClient();
		cmdClient.setLigneCommandeClients(new ArrayList<LigneCommandeClient>());
		
		for(int i=0;i<15;i++) {
			cmdClient.getLigneCommandeClients().add(new LigneCommandeClient());
		}
		
		List<Client> clients = clientService.selectAll();
		List<Article> articles = articleService.selectAll();
		
		if (clients == null) {
			clients = new ArrayList<Client>();
		}
		if (articles == null) {
			articles = new ArrayList<Article>();
		}

		model.addAttribute("cmdClient", cmdClient);
		model.addAttribute("clients", clients);
		model.addAttribute("articles", articles);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "commande_client/commande_client_error";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String enregistrerCommandClient(Model model, CommandeClient cmdClient) {
		if(cmdClient != null) {
			if (cmdClient.getIdCommandeClient() != null) {
				cmdCltService.update(cmdClient);
			} else {if(cmdClient.getClient().getIdClient()!=null){
				List<LigneCommandeClient> lcmdClients = cmdClient.getLigneCommandeClients();
				for (LigneCommandeClient lcmdClient : lcmdClients) {
					if(lcmdClient != null) {
						if(lcmdClient.getArticle() != null) {
					Article article = articleService.getById(lcmdClient.getArticle().getIdArticle());
					if(article.getStock()-lcmdClient.getQte()<0){return "redirect:/commande_client/ereurQuantite";}
					}
						}
				}
				cmdCltService.save(cmdClient);
				SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yy");
				cmdClient.getDateCommande().setTime(new Date().getTime());
				cmdClient.setCode("C_"+cmdClient.getIdCommandeClient()+"_"+formater.format(cmdClient.getDateCommande()));
				cmdCltService.update(cmdClient);
				for (LigneCommandeClient lcmdClient : lcmdClients) {
					if(lcmdClient != null) {
						if(lcmdClient.getArticle() != null) {
					lcmdClient.setCommandeClient(cmdClient);
					ligneCmdCltService.save(lcmdClient);
					Article article = articleService.getById(lcmdClient.getArticle().getIdArticle());
					article.setStock(article.getStock()-lcmdClient.getQte());
					articleService.update(article);
					}
						}
				}}
			}
		}
		return "redirect:/commande_client/";
	}
	
	@RequestMapping(value = "/supprimer/{idCommande}")
	public String supprimerCommande(Model model, @PathVariable Long idCommande) {
		if (idCommande != null) {
			CommandeClient cmd = cmdCltService.getById(idCommande);
			if (cmd != null) {

				List<LigneCommandeClient> lcmdfrs = ligneCmdCltService.selectAll();
				
					for (LigneCommandeClient lcmdfr : lcmdfrs) {
						if(lcmdfr.getCommandeClient().getIdCommandeClient() == idCommande) {
							ligneCmdCltService.remove(lcmdfr.getIdLigneCdeClt());
							
						}}
					cmdCltService.remove(idCommande);
			}
		}
		return "redirect:/commande_client/";
	}
	
}
