package com.stock.mvc;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.stock.mvc.alertes.IStockAlertes;
import com.stock.mvc.entites.Category;
import com.stock.mvc.entites.Client;
import com.stock.mvc.entites.CommandeProp;
import com.stock.mvc.entites.Entropot;
import com.stock.mvc.entites.Prop;
import com.stock.mvc.modules.ICategoryModule;
import com.stock.mvc.modules.IEntropotModule;
import com.stock.mvc.modules.IPdfModule;
import com.stock.mvc.modules.IUploadModule;
import com.stock.mvc.services.ICategoryService;
import com.stock.mvc.services.IClientService;
import com.stock.mvc.services.ICommandePropService;
import com.stock.mvc.services.IEntropotService;
import com.stock.mvc.services.IPropService;

@Controller
@RequestMapping(value = "/prop")
public class PropController {
	
	@Autowired
    ServletContext context;
	
	@Autowired
	private IPropService propService;
	
	@Autowired
	private ICommandePropService commandePropService;
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private  IUploadModule saveFile;
	
	@Autowired
	private  ICategoryService catService;
	
	@Autowired
	private  IEntropotService entropotService;
	
	@Autowired
	private  ICategoryModule catModule;
	
	@Autowired
	private  IEntropotModule entropotModule;
	
	@Autowired
	private  IStockAlertes alerteStock;
	
	@Autowired
	private  IPdfModule pdfModule;
	
	@RequestMapping(value = "/")
	public String prop(Model model) {
		List<Prop> props = propService.selectAll();
		
		if (props == null) {
			props = new ArrayList<Prop>();
		}
		model.addAttribute("props", props);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "prop/prop";
	}
	
	@RequestMapping(value = "/category/{idCategory}")
	public String propByCategory(Model model, @PathVariable Long idCategory) {
		List<Prop> props = propService.selectAll();
		List<Prop> propsbycat = new ArrayList<Prop>();
		if (props == null) {
			props = new ArrayList<Prop>();
		}
		for (Prop prop : props) {
			if (prop.getCategory().getIdCategory() == idCategory) {
				propsbycat.add(prop);
			}
		}
		model.addAttribute("props", propsbycat);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "prop/prop";
	}
	
	@RequestMapping(value = "/entropot/{idEntropot}")
	public String articleByEntropot(Model model, @PathVariable Long idEntropot) {
		List<Prop> props = propService.selectAll();
		List<Prop> propsbyentrepot = new ArrayList<Prop>();
		if (props == null) {
			props = new ArrayList<Prop>();
		}
		for (Prop prop : props) {
			if (prop.getEntropot().getIdEntropot() == idEntropot) {
				propsbyentrepot.add(prop);
			}
		}
		model.addAttribute("props", propsbyentrepot);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "prop/prop";
	}
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterProp(Model model) {
		Prop prop = new Prop();
		List<Category> categories = catService.selectAll();
		List<Entropot> entropots = entropotService.selectAll();
		List<Client> clients = clientService.selectAll();
		if (clients == null) {
			clients = new ArrayList<Client>();
		}if (categories == null) {
			categories = new ArrayList<Category>();
			
		}if(categories.isEmpty()) {
			Category cat = new Category("Tous","Tous");
			catService.save(cat);
			categories.add(cat);
			
		}if (entropots == null) {
			entropots = new ArrayList<Entropot>();
			
		}if(entropots.isEmpty()) {
			Entropot entro = new Entropot("FM6OA","FM6OA");
			entropotService.save(entro);
			entropots.add(entro);
			
		}
		model.addAttribute("prop", prop);
		model.addAttribute("clients", clients);
		model.addAttribute("categories", categories);
		model.addAttribute("entropots", entropots);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "prop/ajouterProp";
	}
	
	@RequestMapping(value = "/addcmdprop/{idProp}")
	public String addCmdProp(Model model, @PathVariable Long idProp) {
		Prop prop = propService.getById(idProp);
		
		if(prop != null) {

			CommandeProp cmdprop = new CommandeProp();
			cmdprop.setProp(prop);
		
		model.addAttribute("cmdprop", cmdprop);
		model.addAttribute("clients", clientService.selectAll());
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "prop/newcmdprop";
		}
		return "redirect:/prop/details/"+idProp;
	}
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerProp(Model model, Prop prop, @RequestParam("file") MultipartFile file) {
		String path = context.getRealPath("/images/prop/");
		if (prop != null) {
			if (file != null && !file.isEmpty()) {
				prop.setPhoto(saveFile.SaveFile(file, path+prop.getCodeProp()+"_"));
					}else {if(prop.getPhoto().isEmpty()) {prop.setPhoto("avatar.png");}}

			
			if (prop.getIdProp() != null) {
				Prop oldprop = propService.getById(prop.getIdProp());
				if((prop.getQte()-oldprop.getQte())!=0) {
					if((oldprop.getReste()+(prop.getQte()-oldprop.getQte()))>=0) {
						prop.setReste(oldprop.getReste()+(prop.getQte()-oldprop.getQte()));
					}else {
						model.addAttribute("prop", prop);
						catModule.insertCategories(model);
						entropotModule.insertEntropots(model);
						alerteStock.insertAlertes(model,alerteStock.alertes());
						return "prop/ajouterProp2";
					}
				}
				propService.update(prop);
				
			} else {
				prop.setReste(prop.getQte());
				propService.save(prop);
			}
		}
		return "redirect:/prop/";
	}
	
	@RequestMapping(value = "/savecmdprop", method = RequestMethod.POST)
	public String saveCmdProp(Model model,CommandeProp cmdprop) {
		if (cmdprop != null) {
			if (cmdprop.getIdCmdProp() != null) {
				cmdprop.setProp(propService.getById(cmdprop.getProp().getIdProp()));
				CommandeProp oldcmdprop = commandePropService.getById(cmdprop.getIdCmdProp());
				if((cmdprop.getProp().getReste()+oldcmdprop.getQte())-cmdprop.getQte()>=0) {
					
					cmdprop.getProp().setReste((cmdprop.getProp().getReste()+oldcmdprop.getQte())-cmdprop.getQte());
					propService.update(cmdprop.getProp());
					commandePropService.update(cmdprop);
				}else {
					model.addAttribute("cmdprop", cmdprop);
					model.addAttribute("clients", clientService.selectAll());
					catModule.insertCategories(model);
					entropotModule.insertEntropots(model);
					alerteStock.insertAlertes(model,alerteStock.alertes());
					return "prop/newcmdprop2";
				}
				
				
			} else {
				cmdprop.setProp(propService.getById(cmdprop.getProp().getIdProp()));
				if(cmdprop.getProp().getReste()-cmdprop.getQte()>=0) {
					
					cmdprop.getProp().setReste(cmdprop.getProp().getReste()-cmdprop.getQte());
					propService.update(cmdprop.getProp());
					commandePropService.save(cmdprop);
				}else {
					model.addAttribute("cmdprop", cmdprop);
					model.addAttribute("clients", clientService.selectAll());
					catModule.insertCategories(model);
					entropotModule.insertEntropots(model);
					alerteStock.insertAlertes(model,alerteStock.alertes());
					return "prop/newcmdprop2";
				}
				
			}
		}
		return "redirect:/prop/details/"+cmdprop.getProp().getIdProp();
	}
	
	@RequestMapping(value = "/details/{idProp}")
	public String detais(Model model, @PathVariable Long idProp) {
		Prop prop = propService.getById(idProp);
		
		if(prop != null) {

			List<CommandeProp> allcmdprops = commandePropService.selectAll();
			List<CommandeProp> cmdprops = new ArrayList<CommandeProp>();
			
			for (CommandeProp commandeProp : allcmdprops) {
				if(commandeProp.getProp().getIdProp() == idProp) {
					cmdprops.add(commandeProp);
				}
			}

		model.addAttribute("prop", prop);	
		model.addAttribute("cmdprops", cmdprops);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "prop/detailprop";
		}
		return "redirect:/prop/";
	}
	
	@RequestMapping(value = "/pdf/{idProp}")
	public String PropA4(Model model, @PathVariable Long idProp) {
		Prop prop = propService.getById(idProp);
		
		if(prop != null) {

			List<CommandeProp> allcmdprops = commandePropService.selectAll();
			List<CommandeProp> cmdprops = new ArrayList<CommandeProp>();
			
			for (CommandeProp commandeProp : allcmdprops) {
				if(commandeProp.getProp().getIdProp() == idProp) {
					cmdprops.add(commandeProp);
				}
			}

		model.addAttribute("prop", prop);	
		model.addAttribute("cmdprops", cmdprops);
		return "prop/detailPropPdf";
		}
		return "redirect:/prop/";
	}
	
	@RequestMapping(value = "/detailspdf/{idProp}")
	public String pdf(Model model, @PathVariable Long idProp) {
		Prop prop = propService.getById(idProp);
		String path = context.getContextPath();if(path.equals("/")) {path="";}
		if(prop != null) {String url = "http://localhost:8080"+path+"/prop/pdf/"+idProp;
			return "redirect:"+pdfModule.getPdf(url);
		}
		return "redirect:/prop/";
	}
	
	@RequestMapping(value = "/modifier/{idProp}")
	public String modifierProp(Model model, @PathVariable Long idProp) {
		if (idProp != null) {
			Prop prop = propService.getById(idProp);
			if (prop != null) {
				model.addAttribute("prop", prop);
				
			}
		}
		List<Client> clients = clientService.selectAll();
		if (clients == null) {
			clients = new ArrayList<Client>();
		}
		model.addAttribute("clients", clients);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "prop/ajouterProp";
	}
	
	@RequestMapping(value = "/supprimer/{idProp}")
	public String supprimerProp(Model model, @PathVariable Long idProp) {
		if (idProp != null) {
			Prop prop = propService.getById(idProp);
			if (prop != null) {
				List<CommandeProp> cmdprops = commandePropService.selectAll();
				for (CommandeProp cmdprop : cmdprops) {
					if(cmdprop.getProp().getIdProp() == idProp) {
						commandePropService.remove(cmdprop.getIdCmdProp());
					}
				}
					propService.remove(idProp);
			}
		}
		return "redirect:/prop/";
	}
	
	@RequestMapping(value = "/deletcmdprop/{idCmdProp}")
	public String deletCmdProp(Model model, @PathVariable Long idCmdProp) {
		long id=0;
		if (idCmdProp != null) {
			CommandeProp cmdprop = commandePropService.getById(idCmdProp);
			id = cmdprop.getProp().getIdProp();
			if (cmdprop != null) {
				cmdprop.getProp().setReste(cmdprop.getProp().getReste()+cmdprop.getQte());
				propService.update(cmdprop.getProp());
				commandePropService.remove(idCmdProp);
			}
		}
		return "redirect:/prop/details/"+id;
	}
	

}
