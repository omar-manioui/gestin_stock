package com.stock.mvc;



import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.stock.mvc.alertes.IStockAlertes;
import com.stock.mvc.entites.Article;
import com.stock.mvc.entites.Cle;
import com.stock.mvc.entites.Client;
import com.stock.mvc.entites.Fournisseur;
import com.stock.mvc.entites.Utilisateur;
import com.stock.mvc.modules.Calendar;
import com.stock.mvc.modules.ICategoryModule;
import com.stock.mvc.modules.IEntropotModule;
import com.stock.mvc.modules.IUploadModule;
import com.stock.mvc.services.IClientService;
import com.stock.mvc.services.IFournisseurService;
import com.stock.mvc.services.IKeyService;
import com.stock.mvc.services.IUtilisateurService;


/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/")
public class HomeController {
	@Autowired
    ServletContext context;
	
	@Autowired
	private  IUploadModule saveFile;
	
	@Autowired
	private IUtilisateurService utilisateurService;
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private IFournisseurService fournisseurService;
	
	@Autowired
	private IKeyService keyService;
	
	@Autowired
	private  ICategoryModule catModule;
	
	@Autowired
	private  IEntropotModule entropotModule;
	
	@Autowired
	private  IStockAlertes alerteStock;
	
	@RequestMapping(value = "/home")
	public String home(Model model) {
		List<Client> clients = clientService.selectAll();
		List<Fournisseur> fournisseurs = fournisseurService.selectAll();
		List<Cle> keys = keyService.selectAll();
		
		if (clients == null) {
			clients = new ArrayList<Client>();
		}
		if (keys == null) {
			keys = new ArrayList<Cle>();
		}
		if (fournisseurs == null) {
			fournisseurs = new ArrayList<Fournisseur>();
		}
		model.addAttribute("keys", keys);
		model.addAttribute("fournisseurs", fournisseurs);
		model.addAttribute("clients", clients);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "home/home";
	}
	@RequestMapping(value = "/alertes")
	public String alertes(Model model) {
		List<Article> articles = alerteStock.alertes();
		
		if (articles == null) {
			articles = new ArrayList<Article>();
		}
		model.addAttribute("articles", articles);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "article/article";
	}
	
	@RequestMapping(value = "/user")
	public String user(Model model) {
		Utilisateur user = utilisateurService.getById((long) 1);
		model.addAttribute("user", user);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "user/detailUser";
	}
	
	@RequestMapping(value = "/edituser")
	public String editUser(Model model) {
		Utilisateur user = utilisateurService.getById((long) 1);
		model.addAttribute("user", user);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "user/editUser";
	}
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerClient(Model model, Utilisateur user, @RequestParam("file") MultipartFile file) {
		String path = context.getRealPath("/images/client/");
		if (user != null) {
			if (file != null && !file.isEmpty()) {
				user.setPhoto(saveFile.SaveFile(file, path+user.getNom()+"_"+user.getPrenom()+"_"));
					}else {if(user.getPhoto().isEmpty()) {user.setPhoto("avatar.png");}}
						

			
			if (user.getIdUtilisateur() != null) {
				utilisateurService.update(user);
			} else {
				utilisateurService.save(user);
			}
		}
		return "redirect:/home/";
	}
	
	@RequestMapping(value = "/footer")
	public String footer() {
		
		return "includes/footer";
	}
	
	@RequestMapping(value = "/header")
	public String header() {
		
		return "includes/header";
	}

	@RequestMapping(value = "/")
	public String login(HttpServletRequest req) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication(); 
		if (!(auth instanceof AnonymousAuthenticationToken)) {
		    return "redirect:/home";
		}
		 return "login/login";
	}
	
	
	@RequestMapping(value = "/failedlogin")
	public String failedlogin() {
		
		return "login/failedlogin";
	}
	
	@RequestMapping(value="/calendar", method={RequestMethod.GET}, produces="application/json")
	    @ResponseBody public String homePageJson() {
		 List<Calendar> l = new ArrayList<Calendar>();
		 Gson gson = new Gson();
		 
		 Calendar c = new Calendar();
		 c.setId(1);
		 c.setColor("red");
		 c.setStart("2017-12-02T10:30:00");
		 c.setEnd("2017-12-02T11:00:00");
		 c.setTitle("sale principale");
		 
		 Calendar a = new Calendar();
		 a.setId(3);
		 a.setColor("bleu");
		 a.setStart("2017-12-02T11:00:00");
		 a.setEnd("2017-12-02T11:20:00");
		 a.setTitle("menage");
		 
		Calendar d = new Calendar();
		 d.setId(2);
		 d.setStart("2017-12-06");
		 d.setEnd("2017-12-08");
		 d.setTitle("Task in Progress");
		 
		 l.add(c);l.add(a);
		l.add(d);
		return gson.toJson(l);
	    }

}