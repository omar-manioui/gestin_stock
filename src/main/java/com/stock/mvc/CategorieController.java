package com.stock.mvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.alertes.IStockAlertes;
import com.stock.mvc.entites.Article;
import com.stock.mvc.entites.Category;
import com.stock.mvc.entites.Prop;
import com.stock.mvc.modules.ICategoryModule;
import com.stock.mvc.modules.IEntropotModule;
import com.stock.mvc.services.IArticleService;
import com.stock.mvc.services.ICategoryService;
import com.stock.mvc.services.IPropService;

@Controller
@RequestMapping(value = "/categorie")
public class CategorieController {
	
	@Autowired
	private IArticleService articleService;
	
	@Autowired
	private IPropService propService;
	
	@Autowired
	private  ICategoryService catService;
	
	@Autowired
	private  ICategoryModule catModule;
	
	@Autowired
	private  IEntropotModule entropotModule;
	
	@Autowired
	private  IStockAlertes alerteStock;
	
	@RequestMapping(value = "/")
	public String categorie(Model model) {
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "categorie/categorie";
	}
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterCategorie(Model model) {
		Category categorie = new Category();
		model.addAttribute("categorie", categorie);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "categorie/ajouterCategorie";
	}
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerCategorie(Model model, Category categorie) {
		
		if (categorie != null) {
			
			
			if (categorie.getIdCategory() != null) {
				catService.update(categorie);
			} else {
				catService.save(categorie);
			}
		}
		return "redirect:/categorie/";
	}
	
	
	@RequestMapping(value = "/modifier/{idCategory}")
	public String modifierArticle(Model model, @PathVariable Long idCategory) {
		if (idCategory != null) {
			Category categorie = catService.getById(idCategory);
			if (categorie != null) {
				model.addAttribute("categorie", categorie);
			}
		}
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "categorie/ajouterCategorie";
	}
	
	@RequestMapping(value = "/supprimer/{idCategory}")
	public String supprimerFournisseur(Model model, @PathVariable Long idCategory) {
		if (idCategory != null) {
			Category categorie = catService.getById(idCategory);
			
			if (categorie != null) {
				if(categorie.getCode().equals("Tous")) {return "redirect:/categorie/";}
				List<Category> cats = catService.selectAll();
				List<Article> articles = articleService.selectAll();
				List<Prop> props = propService.selectAll();
				Category defaultCat = null;
				for (Category cat : cats) {
					if(cat.getCode().equals("Tous")) {
						defaultCat = cat;
					}
				}
				if(defaultCat == null) {
					defaultCat = new Category("Tous","Tous");
					catService.save(defaultCat);
				}
				
					for (Article article : articles) {
						if(article.getCategory().getIdCategory() == idCategory) {
							article.setCategory(defaultCat);
							articleService.update(article);
						}
					}for (Prop prop : props) {
						if(prop.getCategory().getIdCategory() == idCategory) {
							prop.setCategory(defaultCat);
							propService.update(prop);}}
				catService.remove(idCategory);
			}
		}
		return "redirect:/categorie/";
	}
	
	

}
