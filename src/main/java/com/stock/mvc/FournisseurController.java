package com.stock.mvc;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.stock.mvc.alertes.IStockAlertes;
import com.stock.mvc.entites.CommandeFournisseur;
import com.stock.mvc.entites.Fournisseur;
import com.stock.mvc.entites.LigneCommandeFournisseur;
import com.stock.mvc.modules.ICategoryModule;
import com.stock.mvc.modules.IEntropotModule;
import com.stock.mvc.modules.IPdfModule;
import com.stock.mvc.modules.IUploadModule;
import com.stock.mvc.services.ICommandeFournisseurService;
import com.stock.mvc.services.IFournisseurService;
import com.stock.mvc.services.ILigneCommandeFournisseurService;

@Controller
@RequestMapping(value = "/fournisseur")
public class FournisseurController {
	
	@Autowired
    ServletContext context;
	
	@Autowired
	private IFournisseurService fournisseurService;
	
	@Autowired
	private  IUploadModule saveFile;
	
	@Autowired
	private  IStockAlertes alerteStock;
	
	@Autowired
	private  ICategoryModule catModule;
	
	@Autowired
	private  IEntropotModule entropotModule;
	
	@Autowired
	private ILigneCommandeFournisseurService ligneCmdFrsService;
	
	@Autowired
	private ICommandeFournisseurService cmdFrsService;
	
	@Autowired
	private  IPdfModule pdfModule;
	
	@RequestMapping(value = "/")
	public String fournisseur(Model model) {
		List<Fournisseur> fournisseurs = fournisseurService.selectAll();
		
		if (fournisseurs == null) {
			fournisseurs = new ArrayList<Fournisseur>();
		}
		model.addAttribute("fournisseurs", fournisseurs);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "fournisseur/fournisseur";
	}
	
	@RequestMapping(value = "/details/{idFournisseur}")
	public String listArticle(Model model, @PathVariable Long idFournisseur) {
		Fournisseur fournisseur = fournisseurService.getById(idFournisseur);
		if(fournisseur != null) {

			List<LigneCommandeFournisseur> lcmdclns = ligneCmdFrsService.selectAll();
			List<LigneCommandeFournisseur> lcmdarticles = new ArrayList<LigneCommandeFournisseur>();
			
			for (LigneCommandeFournisseur lcmdcln : lcmdclns) {
				if(lcmdcln.getCommandeFournisseur().getFournisseur().getIdFournisseur() == idFournisseur) {
					lcmdarticles.add(lcmdcln);
					
				}
			}

		model.addAttribute("fournisseur", fournisseur);	
		model.addAttribute("lcmdarticles", lcmdarticles);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "fournisseur/detailFournisseur";
		}
		return "redirect:/fournisseur/";
	}
	
	@RequestMapping(value = "/pdf/{idFournisseur}")
	public String fournisseurA4(Model model, @PathVariable Long idFournisseur) {
		Fournisseur fournisseur = fournisseurService.getById(idFournisseur);
		if(fournisseur != null) {

			List<LigneCommandeFournisseur> lcmdclns = ligneCmdFrsService.selectAll();
			List<LigneCommandeFournisseur> lcmdarticles = new ArrayList<LigneCommandeFournisseur>();
			
			for (LigneCommandeFournisseur lcmdcln : lcmdclns) {
				if(lcmdcln.getCommandeFournisseur().getFournisseur().getIdFournisseur() == idFournisseur) {
					lcmdarticles.add(lcmdcln);
					
				}
			}

		model.addAttribute("fournisseur", fournisseur);	
		model.addAttribute("lcmdarticles", lcmdarticles);
		return "fournisseur/detailFournisseurPdf";
		}
		return "redirect:/fournisseur/";
	}
	
	@RequestMapping(value = "/detailspdf/{idFournisseur}")
	public String pdf(Model model, @PathVariable Long idFournisseur) {
		Fournisseur fournisseur = fournisseurService.getById(idFournisseur);
		String path = context.getContextPath();if(path.equals("/")) {path="";}
		if(fournisseur != null) {String url = "http://localhost:8080"+path+"/fournisseur/pdf/"+idFournisseur;
			return "redirect:"+pdfModule.getPdf(url);
		}
		return "redirect:/fournisseur/";
	}
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterFournisseur(Model model) {
		Fournisseur fournisseur = new Fournisseur();
		model.addAttribute("fournisseur", fournisseur);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "fournisseur/ajouterFournisseur";
	}
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerFournisseur(Model model, Fournisseur fournisseur, @RequestParam("file") MultipartFile file) {
		String path = context.getRealPath("/images/fournisseur/");
		if (fournisseur != null) {
			if (file != null && !file.isEmpty()) {
				fournisseur.setPhoto(saveFile.SaveFile(file, path+fournisseur.getNom()+"_"+fournisseur.getPrenom()+"_"));
					}else {if(fournisseur.getPhoto().isEmpty()) {
						if(fournisseur.getGnr().equals("Ste")){fournisseur.setPhoto("avatarste.png");
						}else if(fournisseur.getGnr().equals("Mme") || fournisseur.getGnr().equals("Mlle")){fournisseur.setPhoto("avatara.png");
						}else {fournisseur.setPhoto("avatar.png");}}}

			
			if (fournisseur.getIdFournisseur() != null) {
				fournisseurService.update(fournisseur);
			} else {
				fournisseurService.save(fournisseur);
			}
		}
		return "redirect:/fournisseur/";
	}
	
	
	@RequestMapping(value = "/modifier/{idFournisseur}")
	public String modifierFournisseur(Model model, @PathVariable Long idFournisseur) {
		if (idFournisseur != null) {
			Fournisseur fournisseur = fournisseurService.getById(idFournisseur);
			if (fournisseur != null) {
				model.addAttribute("fournisseur", fournisseur);
			}
		}
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "fournisseur/ajouterFournisseur";
	}
	
	@RequestMapping(value = "/supprimer/{idFournisseur}")
	public String supprimerFournisseur(Model model, @PathVariable Long idFournisseur) {
		if (idFournisseur != null) {
			Fournisseur fournisseur = fournisseurService.getById(idFournisseur);
			if (fournisseur != null) {

				List<LigneCommandeFournisseur> lcmdfrs = ligneCmdFrsService.selectAll();
				List<CommandeFournisseur> cmdfrs = cmdFrsService.selectAll();
				
					for (LigneCommandeFournisseur lcmdfr : lcmdfrs) {
						if(lcmdfr.getCommandeFournisseur().getFournisseur().getIdFournisseur() == idFournisseur) {
							ligneCmdFrsService.remove(lcmdfr.getIdLigneCdeFrs());
							
						}}
					for (CommandeFournisseur cmdfr : cmdfrs) {
						if(cmdfr.getFournisseur().getIdFournisseur() == idFournisseur) {
							cmdFrsService.remove(cmdfr.getIdCommandeFournisseur());
						}
					}
				fournisseurService.remove(idFournisseur);
			}
		}
		return "redirect:/fournisseur/";
	}
	
	

}
