package com.stock.mvc;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.alertes.IStockAlertes;
import com.stock.mvc.entites.CommandeKey;
import com.stock.mvc.entites.Cle;
import com.stock.mvc.modules.ICategoryModule;
import com.stock.mvc.modules.IEntropotModule;
import com.stock.mvc.modules.IPdfModule;
import com.stock.mvc.services.IClientService;
import com.stock.mvc.services.ICommandeKeyService;
import com.stock.mvc.services.IKeyService;

@Controller
@RequestMapping(value = "/keys")
public class KeyController {
	
	@Autowired
    ServletContext context;
	
	@Autowired
	private IKeyService keyService;
	
	@Autowired
	private ICommandeKeyService commandeKeyService;
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private  ICategoryModule catModule;
	
	@Autowired
	private  IEntropotModule entropotModule;
	
	@Autowired
	private  IStockAlertes alerteStock;
	
	@Autowired
	private  IPdfModule pdfModule;
	
	@RequestMapping(value = "/")
	public String keys(Model model) {
		List<Cle> cles = keyService.selectAll();
		
		if (cles == null) {
			cles = new ArrayList<Cle>();
		}
		model.addAttribute("cles", cles);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "key/keys";
	}
	
	@RequestMapping(value = "/newkey", method = RequestMethod.GET)
	public String addKey(Model model) {
		Cle cle = new Cle();
		cle.setCmdKey(new ArrayList<CommandeKey>());
		
		model.addAttribute("cle", cle);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "key/newkey";
	}
	
	@RequestMapping(value = "/addcmdkey/{idCle}")
	public String addCmdKey(Model model, @PathVariable Long idCle) {
		Cle cle = keyService.getById(idCle);
		
		if(cle != null) {

			CommandeKey cmdkey = new CommandeKey();
			cmdkey.setCle(cle);
		
		model.addAttribute("cmdkey", cmdkey);
		model.addAttribute("clients", clientService.selectAll());
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "key/newcmdkey";
		}
		return "redirect:/keys/keydetails/"+idCle;
	}
	
	@RequestMapping(value = "/savekey", method = RequestMethod.POST)
	public String saveKey(Model model, Cle cle) {

		if (cle != null) {
			if (cle.getIdCle() != null) {
				Cle oldKey = keyService.getById(cle.getIdCle());
				if((cle.getQte()-oldKey.getQte())!=0) {
					if((oldKey.getReste()+(cle.getQte()-oldKey.getQte()))>=0) {
						cle.setReste(oldKey.getReste()+(cle.getQte()-oldKey.getQte()));
					}else {
						model.addAttribute("cle", cle);
						catModule.insertCategories(model);
						entropotModule.insertEntropots(model);
						alerteStock.insertAlertes(model,alerteStock.alertes());
						return "key/newkey2";
					}
				}
				keyService.update(cle);
				
			} else {
				cle.setReste(cle.getQte());
				keyService.save(cle);
			}
		}
		return "redirect:/keys/";
	}
	
	@RequestMapping(value = "/savecmdkey", method = RequestMethod.POST)
	public String saveCmdKey(Model model,CommandeKey cmdkey) {
		if (cmdkey != null) {
			if (cmdkey.getIdCmdKey() != null) {
				cmdkey.setCle(keyService.getById(cmdkey.getCle().getIdCle()));
				CommandeKey oldcmdkey = commandeKeyService.getById(cmdkey.getIdCmdKey());
				if((cmdkey.getCle().getReste()+oldcmdkey.getQte())-cmdkey.getQte()>=0) {
					
					cmdkey.getCle().setReste((cmdkey.getCle().getReste()+oldcmdkey.getQte())-cmdkey.getQte());
					keyService.update(cmdkey.getCle());
					commandeKeyService.update(cmdkey);
				}else {
					model.addAttribute("cmdkey", cmdkey);
					model.addAttribute("clients", clientService.selectAll());
					catModule.insertCategories(model);
					entropotModule.insertEntropots(model);
					alerteStock.insertAlertes(model,alerteStock.alertes());
					return "key/newcmdkey2";
				}
				
				
			} else {
				cmdkey.setCle(keyService.getById(cmdkey.getCle().getIdCle()));
				if(cmdkey.getCle().getReste()-cmdkey.getQte()>=0) {
					
					cmdkey.getCle().setReste(cmdkey.getCle().getReste()-cmdkey.getQte());
					keyService.update(cmdkey.getCle());
					commandeKeyService.save(cmdkey);
				}else {
					model.addAttribute("cmdkey", cmdkey);
					model.addAttribute("clients", clientService.selectAll());
					catModule.insertCategories(model);
					entropotModule.insertEntropots(model);
					alerteStock.insertAlertes(model,alerteStock.alertes());
					return "key/newcmdkey2";
				}
				
			}
		}
		return "redirect:/keys/keydetails/"+cmdkey.getCle().getIdCle();
	}
	
	@RequestMapping(value = "/keydetails/{idCle}")
	public String detaisKey(Model model, @PathVariable Long idCle) {
		Cle cle = keyService.getById(idCle);
		
		if(cle != null) {

			List<CommandeKey> allcmdkeys = commandeKeyService.selectAll();
			List<CommandeKey> cmdkeys = new ArrayList<CommandeKey>();
			
			for (CommandeKey commandeKey : allcmdkeys) {
				if(commandeKey.getCle().getIdCle() == idCle) {
					cmdkeys.add(commandeKey);
				}
			}

		model.addAttribute("cle", cle);	
		model.addAttribute("cmdkeys", cmdkeys);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "key/detailKey";
		}
		return "redirect:/keys/";
	}
	
	@RequestMapping(value = "/pdf/{idCle}")
	public String KeyA4(Model model, @PathVariable Long idCle) {
		Cle cle = keyService.getById(idCle);
		
		if(cle != null) {

			List<CommandeKey> allcmdkeys = commandeKeyService.selectAll();
			List<CommandeKey> cmdkeys = new ArrayList<CommandeKey>();
			
			for (CommandeKey commandeKey : allcmdkeys) {
				if(commandeKey.getCle().getIdCle() == idCle) {
					cmdkeys.add(commandeKey);
				}
			}

		model.addAttribute("cle", cle);	
		model.addAttribute("cmdkeys", cmdkeys);
		return "key/detailKeyPdf";
		}
		return "redirect:/keys/";
	}
	
	@RequestMapping(value = "/detailspdf/{idCle}")
	public String pdf(Model model, @PathVariable Long idCle) {
		Cle cle = keyService.getById(idCle);
		String path = context.getContextPath();if(path.equals("/")) {path="";}
		if(cle != null) {String url = "http://localhost:8080"+path+"/keys/pdf/"+idCle;
			return "redirect:"+pdfModule.getPdf(url);
		}
		return "redirect:/keys/";
	}
	
	@RequestMapping(value = "/editkey/{idCle}")
	public String editKey(Model model, @PathVariable Long idCle) {
		if (idCle != null) {
			Cle cle = keyService.getById(idCle);
			if (idCle != null) {
				model.addAttribute("cle", cle);
				
			}
		}
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "key/newkey";
	}
	
	@RequestMapping(value = "/deletkey/{idCle}")
	public String deletKey(Model model, @PathVariable Long idCle) {
		if (idCle != null) {
			Cle cle = keyService.getById(idCle);
			if (cle != null) {
				List<CommandeKey> cmdkeys = commandeKeyService.selectAll();
				for (CommandeKey cmdkey : cmdkeys) {
					if(cmdkey.getCle().getIdCle() == idCle) {
						commandeKeyService.remove(cmdkey.getIdCmdKey());
					}
				}
					keyService.remove(idCle);
			}
		}
		return "redirect:/keys/";
	}
	
	@RequestMapping(value = "/deletcmdkey/{idCmdKey}")
	public String deletCmdKey(Model model, @PathVariable Long idCmdKey) {
		long id=0;
		if (idCmdKey != null) {
			CommandeKey cmdkey = commandeKeyService.getById(idCmdKey);
			id = cmdkey.getCle().getIdCle();
			if (cmdkey != null) {
				cmdkey.getCle().setReste(cmdkey.getCle().getReste()+cmdkey.getQte());
				keyService.update(cmdkey.getCle());
					commandeKeyService.remove(idCmdKey);
			}
		}
		return "redirect:/keys/keydetails/"+id;
	}
	
	

}
