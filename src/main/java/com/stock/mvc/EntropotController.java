package com.stock.mvc;




import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.alertes.IStockAlertes;
import com.stock.mvc.entites.Article;
import com.stock.mvc.entites.Entropot;
import com.stock.mvc.entites.Prop;
import com.stock.mvc.modules.ICategoryModule;
import com.stock.mvc.modules.IEntropotModule;
import com.stock.mvc.services.IArticleService;
import com.stock.mvc.services.IEntropotService;
import com.stock.mvc.services.IPropService;

@Controller
@RequestMapping(value = "/entropot")
public class EntropotController {
	
	@Autowired
	private IPropService propService;
	
	@Autowired
	private IArticleService articleService;
	
	@Autowired
	private  IEntropotService entropotService;
	
	@Autowired
	private  ICategoryModule catModule;
	
	@Autowired
	private  IEntropotModule entropotModule;
	
	@Autowired
	private  IStockAlertes alerteStock;
	
	@RequestMapping(value = "/")
	public String entropot(Model model) {
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "entropot/entropot";
	}
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterEntropot(Model model) {
		Entropot entropot = new Entropot();
		model.addAttribute("entropot", entropot);
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "entropot/ajouterEntropot";
	}
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerCategorie(Model model, Entropot entropot) {
		
		if (entropot != null) {
			
			
			if (entropot.getIdEntropot() != null) {
				entropotService.update(entropot);
			} else {
				entropotService.save(entropot);
			}
		}
		return "redirect:/entropot/";
	}
	
	
	@RequestMapping(value = "/modifier/{idEntropot}")
	public String modifierArticle(Model model, @PathVariable Long idEntropot) {
		if (idEntropot != null) {
			Entropot entropot = entropotService.getById(idEntropot);
			if (entropot != null) {
				model.addAttribute("entropot", entropot);
			}
		}
		catModule.insertCategories(model);
		entropotModule.insertEntropots(model);
		alerteStock.insertAlertes(model,alerteStock.alertes());
		return "entropot/ajouterEntropot";
	}
	
	@RequestMapping(value = "/supprimer/{idEntropot}")
	public String supprimerFournisseur(Model model, @PathVariable Long idEntropot) {
		if (idEntropot != null) {
			Entropot entropot = entropotService.getById(idEntropot);
			
			if (entropot != null) {
				if(entropot.getCode().equals("FM6OA")) {return "redirect:/entropot/";}
				List<Entropot> cats = entropotService.selectAll();
				List<Article> articles = articleService.selectAll();
				List<Prop> props = propService.selectAll();
				Entropot defaultCat = null;
				for (Entropot cat : cats) {
					if(cat.getCode().equals("FM6OA")) {
						defaultCat = cat;
					}
				}
				if(defaultCat == null) {
					defaultCat = new Entropot("FM6OA","FM6OA");
					entropotService.save(defaultCat);
				}
				
					for (Article article : articles) {
						if(article.getEntropot().getIdEntropot() == idEntropot) {
							article.setEntropot(defaultCat);
							articleService.update(article);
						}
					}for (Prop prop : props) {
						if(prop.getEntropot().getIdEntropot() == idEntropot) {
							prop.setEntropot(defaultCat);
							propService.update(prop);}}
					entropotService.remove(idEntropot);
			}
		}
		
		return "redirect:/entropot/";
		
	}
	
	

}
